/**
 * application/x-www-form-urlencoded
 * @param params
 * @returns {string}
 */
function getHttpBuildUrl(params) {

    let data = [];
    for (let key in params) {
        let item = key + '=' + encodeURIComponent(params[key]);
        data.push(item);
    }

    return data.join('&');
}

/**
 * @param form Object
 * @return Object
 */
function getInputParams(form) {

    let params = {};

    let formElements = form.elements;
    for (let i = 0; i < formElements.length; i++) {
        let formElement = formElements[i];
        if (formElement.nodeName === 'INPUT') {
            params[formElement.name] = formElement.value;
        }
    }

    return params;
}

function sendPost(url, body, callback) {

    let xhr = new XMLHttpRequest();
    xhr.open('POST', url, true);
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');

    xhr.send(body);

    xhr.onreadystatechange = function () {

        if (xhr.readyState === 4) {
            if (xhr.status === 200) {
                callback(xhr.responseText);
            } else {
                console.log(xhr.status + ': ' + xhr.statusText);
            }
        }
    };
}

function hideElement(element, callback) {
    let elStyle = element.style;
    elStyle.opacity = 1;
    (function fade(){
        (elStyle.opacity -= .1) < 0 ? elStyle.display = 'none' : setTimeout(fade, 40);

        if (elStyle.opacity < 0 && callback) {
            callback();
        }
    })();
}
function invisibleElement(element, callback) {
    let elStyle = element.style;
    elStyle.opacity = 1;
    (function fade(){
        (elStyle.opacity -= .1) < 0 ? elStyle.display = 'block' : setTimeout(fade, 40);

        if (elStyle.opacity < 0 && callback) {
            callback();
        }
    })();
}
function showElement(element) {

    let elStyle = element.style;
    elStyle.opacity = 0;
    (function fade(){
        elStyle.opacity = elStyle.opacity * 1 + .1;
        (elStyle.opacity > 1) ? elStyle.display = 'block' : setTimeout(fade, 40);
    })();
}

function setFormHandler(formId) {
    let form = document.getElementById(formId);
    form.addEventListener('keydown', (e) => {
        if(e.code === 'Enter') {
            e.preventDefault();
            return false;
        }
    });
    form.addEventListener('submit', (e) => {
        e.preventDefault();
        let params = getInputParams(form);
        sendPost(form.action, getHttpBuildUrl(params), function(response) {
            let data = JSON.parse(response);
            if (data.status === 'OK') {
                form.reset();
                handleModal(data.message, 'colorGreen');
            } else {
                handleModal(data.message, 'colorRed');
            }
        });
    });
}
