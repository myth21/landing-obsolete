<?php

if (PHP_SAPI === 'cli') {

    $login = $_SERVER['argv'][1];
    $passwd = $_SERVER['argv'][2];
    $htpasswdFile = __DIR__ . DIRECTORY_SEPARATOR . '.htpasswd';

    if (!is_writeable($htpasswdFile)) {
        exit('File ' . $htpasswdFile . ' is not writeable');
    }

    if (!$passwd) {
        exit('Please set passwd param');
    }

    $passwordHash = password_hash($passwd, PASSWORD_DEFAULT);

    $bytes = file_put_contents($htpasswdFile, PHP_EOL . $login . ':' . $passwordHash, FILE_APPEND);

    echo $bytes ? 'OK' : 'FAIL';
}