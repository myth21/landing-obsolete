<?php

return [
    'appViewRoot' => __DIR__ . DIRECTORY_SEPARATOR . 'app' . DIRECTORY_SEPARATOR . 'view' . DIRECTORY_SEPARATOR,
    'defaultLayoutName' => 'layout_default',
    'defaultControllerName' => 'Index',
    'defaultActionName' => 'index',
    'consoleControllerNameSpace' => '\\app\\console\\',
    'webControllerNameSpace' => '\\app\\controller\\',
    'exceptionControllerName' => 'Exception',
    'exceptionMethodName' => 'handle',
];
