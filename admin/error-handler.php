<?php

ini_set('display_errors', $params['displayErrors']);

set_error_handler(function ($level, $message, $file, $line) use ($params) {

    echo "Sorry, there is an error" . PHP_EOL;

    $message .= PHP_EOL . PHP_EOL;
    $message .= $file . ':' . $line;

    if ($params['displayErrors']) {
        echo $message;
    }

    $headers = [
        //'From' => 'Developer <developer@company.com>',
        'Content-type' => 'text/plain; charset="utf-8"',
    ];

    mail($params['adminEmail'], 'Error', $message, $headers);

}, E_ALL);
