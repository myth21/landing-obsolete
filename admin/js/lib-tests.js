/**
 * https://api.qunitjs.com/
 */
QUnit.test('testGetHttpBuildUrl', function(assert) {
    let httpBuildUrl = getHttpBuildUrl({});
    assert.equal(httpBuildUrl, '', '');
});
QUnit.test('testGetHttpBuildUrl', function(assert) {
    let httpBuildUrl = getHttpBuildUrl({
        'name':'value',
    });
    assert.equal(httpBuildUrl, 'name=value', '');
});
QUnit.test('testGetHttpBuildUrl', function(assert) {
    let httpBuildUrl = getHttpBuildUrl({
        'name':'value',
        'name2':'value2',
    });
    assert.equal(httpBuildUrl, 'name=value&name2=value2', '');
});

QUnit.test('testSendPost', function(assert) {
    let asyncDone = assert.async();
    let url = 'http://jsonplaceholder.typicode.com/posts';
    let body = '';
    let isResponseOk = false;
    let callBack = function (response) {
        isResponseOk = JSON.parse(response).id === 101;
    };
    sendPost(url, body, callBack);
    setTimeout(function() {
        assert.ok(isResponseOk);
        asyncDone();
    }, 1000);
});