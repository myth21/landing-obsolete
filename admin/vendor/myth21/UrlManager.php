<?php
namespace vendor\myth21;

trait UrlManager
{
    protected $controllerKey = 'controller';
    protected $actionKey = 'action';

    public function getControllerKey()
    {
        return $this->controllerKey;
    }

    public function getActionKey()
    {
        return $this->actionKey;
    }

    public function createUrl($controller = '', $action = '', $params = [])
    {
        if (!$controller && !$action && !$params) {
            return '';
        }

        $url = '?';
        if ($controller) {
            $url .= $this->controllerKey . '=' . $controller;
            if ($action) {
                $url .= '&'.$this->actionKey. '=' . $action;
                if ($params) {
                    foreach ($params as $key => $value) {
                        $url .= '&' . $key . '=' . $value;
                    }
                }
            }
        }

        return $url;
    }
}