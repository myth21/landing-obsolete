<?php
namespace vendor\myth21;

class ResponseHeader
{
    private $headers = [];
    private $code = 200;
    private $message = '';

    public function setStatus($code, $message = '')
    {
        $this->code = $code;
        $this->message = $message;
    }

    public function addHeader(string $header)
    {
        $this->headers[] = $header;
    }

    public function sendHeaders()
    {
        if (headers_sent()) {
            return;
        }

        foreach ($this->headers as $header) {
            header("{$header}");
        }

        $serverProtocol = $_SERVER['SERVER_PROTOCOL'] ?? 'HTTP/1.0';
        header($serverProtocol . ' ' . $this->code . ' ' . $this->message);
    }

    public function redirect($to = '', $code = 301): void
    {
        header('Location:' . $to, true, $code);
        exit();
    }

    public function getHeaders(): array
    {
        return $this->headers;
    }

}