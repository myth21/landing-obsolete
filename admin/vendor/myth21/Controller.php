<?php
namespace vendor\myth21;

/**
 * Class Controller
 * @package vendor\myth21
 * @var App $app
 */
abstract class Controller
{
    protected $app;

    public function __construct(App $app)
    {
        $this->app = $app;
    }

}