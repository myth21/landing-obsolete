<?php
namespace vendor\myth21;

/**
 * Class App
 * @package vendor\myth21
 * @property array $params
 * @property array $requestParams
 * @property string $controllerName
 * @property string $actionName
 * @property \Throwable $exception
 */
abstract class App
{
    use UrlManager;

    protected $params = [];
    protected $requestParams = [];
    protected $controllerName = '';
    protected $actionName = '';
    protected $exception = null;

    abstract public function setRequestParams();
    abstract protected function getControllerNameSpace();
    abstract protected function out($out);

    private function __construct(array $params)
    {
        $this->params = $params;
    }

    static public function factory(array $params)
    {
        if (PHP_SAPI === 'cli') {
            return new AppConsole($params);
        }

        return new AppWeb($params);
    }

    protected function setControllerName($name)
    {
        $this->controllerName = $name;
    }

    public function getRequestControllerName()
    {
        return $this->requestParams[$this->getControllerKey()] ?? $this->getParam('defaultControllerName');
    }

    public function getRequestActionName()
    {
        return $this->requestParams[$this->getActionKey()] ?? $this->getParam('defaultActionName');
    }

    private function getControllerName(): string
    {
        return $this->controllerName;
    }

    protected function getControllerClassName(): string
    {
        return $this->getControllerNameSpace() . $this->getControllerName() . ucfirst($this->getControllerKey());
    }

    protected function setActionName(string $name)
    {
        $this->actionName = $name;
    }

    protected function getActionName(): string
    {
        return $this->actionName;
    }

    protected function createController(): Controller
    {
        $controllerClassName = $this->getControllerClassName();
        $controller = new $controllerClassName($this);
        $this->controller = $controller;

        return $this->controller;
    }

    public function getParam($name)
    {
        return $this->params[$name];
    }

    public function getParams()
    {
        return $this->params;
    }

    public function getRequestParams()
    {
        return $this->requestParams;
    }

    protected function setException(\Throwable $e): void
    {
        $this->exception = $e;
    }

    public function getException(): \Throwable
    {
        return $this->exception;
    }

    public function run()
    {
        $this->setRequestParams();

        $this->setControllerName($this->getRequestControllerName());
        $this->setActionName($this->getRequestActionName());

        try {
            if (!$this->isActionAvailableToRun()) {
                throw new \BadMethodCallException('Action is not available to run');
            }
            $out = $this->runController();

        } catch (\Throwable $e) { // set_exception_handler([$this, 'exception_handler']);
            $this->setException($e);
            $this->setControllerName($this->getParam('exceptionControllerName'));
            $this->setActionName($this->getParam('exceptionMethodName'));
            $out = $this->runController();
        }

        $this->out($out);
    }

    protected function isActionAvailableToRun()
    {
        // temp, allow controller name with upper case of first letter only
        $isFirstSymbolUppercase = ctype_upper(mb_substr($this->controllerName, 0, 1));
        if (!$isFirstSymbolUppercase) {
            return false;
        }

        if (!method_exists($this->getControllerClassName(), $this->getActionName())) {
            return false;
        }

        return true;
    }
}