<?php
namespace vendor\myth21;

abstract class ConsoleController extends Controller
{
    protected $exitCode = 0;

    public function setExitCode(int $value): void
    {
        $this->exitCode = $value;
    }

    public function getExitCode(): int
    {
        return $this->exitCode;
    }

}