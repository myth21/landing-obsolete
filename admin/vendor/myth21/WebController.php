<?php
namespace vendor\myth21;

/**
 * Class WebController
 * @package vendor\myth21
 * @package Response $response
 */
abstract class WebController extends Controller
{
    protected $response;

    public function createResponse(): void
    {
        $this->response = new ResponseHeader();
    }

    public function getResponse(): ResponseHeader
    {
        return $this->response;
    }


}