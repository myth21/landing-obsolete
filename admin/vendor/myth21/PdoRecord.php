<?php
namespace vendor\myth21;

/**
 * @package vendor\myth21
 */
abstract class PdoRecord extends Pdo
{
    /**
     * Search unambiguous table row by key
     */
    abstract public function getPrimaryKey();

    public function init(array $data = [])
    {
        foreach ($data as $key => $value) {
            if (in_array($key, $this->getAvailableAttributes())) {
                $this->{$key} = $value;
            }
        }
    }

    /**
     * Attributes are available to update or insert column values
     */
    public function getAvailableAttributes(): array
    {
        return array_keys($this->availableAttributes);
    }

    public function getLabel(string $attr): string
    {
        $attributes = $this->availableAttributes;

        return $attributes[$attr] ?? '';
    }

    public function isNew()
    {
        return is_null($this->getPrimaryKey()) ? true : false;
    }

    private function handleSql($sql)
    {
        $pdoStatement = $this->pdo->prepare($sql);
        $pdoStatement->setFetchMode(\PDO::FETCH_CLASS, get_class($this), [$this->dsn]); // \PDO::FETCH_CLASS creates instance and init its
        $pdoStatement->execute();

        return $pdoStatement;
    }

    public function selectPrimary(int $id)
    {
        $sql = 'SELECT * FROM '.$this->getTableName().' WHERE id='.$id;
        $pdoStatement = $this->handleSql($sql);

        return $pdoStatement->fetch();
    }

    public function selectList(array $params = []): array
    {
        $where = isset($params['where']) ? 'WHERE ' . $params['where'] : '';
        $order = isset($params['order']) ? 'ORDER BY ' . $params['order'] : '';

        $sql = 'SELECT * FROM ' . $this->getTableName() . ' ' . $where . ' ' . $order;
        $pdoStatement = $this->handleSql($sql);

        $models = [];
        while($model = $pdoStatement->fetch()) {
            $models[$model->getPrimaryKey()] = $model;
        }

        return $models;
    }

    public function selectOne(array $params = [])
    {
        $list = $this->selectList($params);

        return $list ? array_shift($list) : null;
    }

    public function save()
    {
        if ($this->isNew()) {
            return $this->insert();
        }

        return $this->update();
    }

    public function insert()
    {
        $sql = 'INSERT INTO '.$this->getTableName().' ('.$this->getImplodeAttributes().') VALUES ('.$this->getImplodeAttributes(true).')';
        $pdoStatement = $this->pdo->prepare($sql);

        return $pdoStatement->execute($this->getInsertValues());
    }

    private function getInsertValues(): array
    {
        $out = [];
        foreach ($this as $attr => $value) {
            if (in_array($attr, $this->getAvailableAttributes())) {
                $out[$attr] = $value;
            }
        }
        return $out;
    }

    private function getImplodeAttributes($isAddBindSeparator = false): string
    {
        $out = '';
        foreach ($this as $attr => $value) {
            if (in_array($attr, $this->getAvailableAttributes())) {

                $out .= $isAddBindSeparator ? ':' . $attr . ',' : $attr . ',';
            }
        }
        return substr($out, 0, -1);
    }

    public function update()
    {
        // TODO using bindParam()
        //$sql = "UPDATE add_rows_table SET order_num=? WHERE id_client=? AND order_num=?";
        //$stmt = $pdo->prepare($sql);
        //$stmt->execute([$test_order_num, $id_client, $order_num]);

        $sql = 'UPDATE '.$this->getTableName().' SET '.$this->getUpdateValues().' WHERE id='. $this->id.'';
        $pdoStatement = $this->pdo->prepare($sql);

        return $pdoStatement->execute();
    }

    public function delete()
    {
        $sql = 'DELETE FROM '.$this->getTableName().' WHERE id=:id';
        $pdoStatement = $this->pdo->prepare($sql);
        $pdoStatement->bindParam(':id', $this->id);

        return $pdoStatement->execute();
    }

    private function getUpdateValues()
    {
        $out = '';
        foreach ($this as $attr => $value) {
            if (in_array($attr, $this->getAvailableAttributes())) {
                $out .= $attr . '="' . $value . '"' . ',';
                //$out .= \SQLite3::escapeString($out); // TODO using bindParam()
            }
        }
        return substr($out, 0, -1);
    }

}