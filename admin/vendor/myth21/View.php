<?php
namespace vendor\myth21;

class View
{
    use UrlManager;

    private $viewDir = '';
    private $layoutName = '';
    private $layoutParts = [];
    private $title = '';
    private $metaTags = [];
    private $content = '';
    private $webRoot = '';

    public function setWebRoot(string $webRoot): void
    {
        $this->webRoot = $webRoot;
    }

    public function getWebRoot(): string
    {
        return $this->webRoot;
    }

    public function setViewDir($name): void
    {
        $this->viewDir = $name;
    }

    public function setLayout($name): void
    {
        $this->layoutName = $name;
    }

    public function addLayoutPart(string $name, string $content): void
    {
        $this->layoutParts[$name] = $content;
    }

    public function delLayoutPart(string $name): void
    {
        unset($this->layoutParts[$name]);
    }

    public function getLayoutPart(string $name)
    {
        return $this->layoutParts[$name];
    }

    public function addMetaTags($name, $content): void
    {
        $this->metaTags[$name] = $content;
    }

    public function getMetaTags(): array
    {
        return $this->metaTags;
    }

    public function setTitle(string $value): void
    {
        $this->title = $value;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getContent()
    {
        return $this->content;
    }

    public function renderPart(string $name, array $data = []): string
    {
        $viewFilePath = $this->viewDir . $name . '.php';
        ob_start();
        ob_implicit_flush(false);
        extract($data);
        require_once $viewFilePath;

        return ob_get_clean();
    }

    public function render(string $name, array $data = []): string
    {
        $this->content = $this->renderPart($name, $data);

        $viewFilePath = $this->viewDir . $this->layoutName . '.php';
        ob_start();
        ob_implicit_flush(false);
        extract($data, EXTR_OVERWRITE);
        require_once $viewFilePath;

        return ob_get_clean();
    }

}