<?php
namespace vendor\myth21;

abstract class Pdo
{
    /**
     * @var \PDO|null
     */
    protected $pdo = null;
    protected $dsn = '';

    /**
     * This constructor is called by PDO on action
     * $pdoStatement->setFetchMode(\PDO::FETCH_CLASS, Model::class, [$this->getDsn()]);
     *
     * @param string $dsn
     */
    public function __construct(string $dsn = '')
    {
        if ($dsn) {
            $this->dsn = $dsn;
            $this->pdo = new \PDO($this->dsn);
            $this->pdo->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
        }

    }

    abstract public function getTableName();
}