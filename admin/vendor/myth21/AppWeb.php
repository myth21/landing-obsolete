<?php
namespace vendor\myth21;

/**
 * Class AppWeb
 * @property WebController $controller
 */
class AppWeb extends App
{
    protected $controller = null;

    public function setRequestParams()
    {
        $this->requestParams = $_REQUEST;
    }

    protected function out($out)
    {
        $this->controller->getResponse()->sendHeaders();

        echo $out;
    }

    protected function getControllerNameSpace(): string
    {
        return $this->params['webControllerNameSpace'];
    }

    protected function runController()
    {
        $this->createController();
        $this->controller->createResponse();
        return $this->controller->{$this->actionName}();
    }

}