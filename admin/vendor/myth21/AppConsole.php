<?php
namespace vendor\myth21;

/**
 * Class AppWeb
 * @property ConsoleController $controller
 */
class AppConsole extends App
{
    protected $controller = null;

    protected function getControllerNameSpace(): string
    {
        return $this->getParam('consoleControllerNameSpace');
    }

    public function setRequestParams()
    {
        $params = [];
        foreach ($_SERVER['argv'] as $value) {
            $explodedValue = explode('=', $value);

            if (sizeof($explodedValue) === 2) {
                $key = $explodedValue[0];
                $value = $explodedValue[1];
                $params[$key] = $value;
            }
        }

        $this->requestParams = $params;
    }

    protected function runController()
    {
        $this->createController();
        $this->controller->{$this->actionName}();
        return $this->controller->getExitCode();
    }

    protected function out($out)
    {
        exit($out);
    }

}

