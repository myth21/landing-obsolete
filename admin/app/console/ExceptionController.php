<?php
namespace app\console;

use vendor\myth21\ConsoleController;

class ExceptionController extends ConsoleController
{
    public function handle()
    {
        $out = '';
        $out .= $this->app->getException()->getMessage();
        $out .= PHP_EOL;
        $out .= $this->app->getException()->getFile() . ':' . $this->app->getException()->getLine();
        $out .= PHP_EOL;

        print_r($out);

        $this->setExitCode(-1);

        return $this->getExitCode();
    }

}