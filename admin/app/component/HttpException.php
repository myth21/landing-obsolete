<?php
namespace app\component;

class HttpException extends \Exception
{
    public function __construct($code = 0, $message = '', \Throwable $previous = null)
    {
        if ($code) {
            $message = $this->getCustomMessage($code);
        }

        parent::__construct($message, $code, $previous);
    }

    public function getCustomMessage($code)
    {
        $message = '';

        switch ($code) {
            case 400: $message = 'Bad Request'; break;
            case 404: $message = 'Not Found'; break;
            case 500: $message = 'Internal Server Error'; break;
        }

        return $message;
    }
}