<?php
namespace app\model;

use vendor\myth21\PdoRecord;

class Task extends PdoRecord
{
    protected $id;
    protected $name;
    protected $desc;
    protected $priority;
    protected $project_id =0;
    protected $person_id;

    public $persons = [];
    public $project = null;

    protected $availableAttributes = [
        'name' => 'Name',
        'desc' => 'Desc',
        'priority' => 'Priority',
        'project_id' => 'Project',
        'person_id' => 'Person',
    ];

    public function getTableName()
    {
        return 'task';
    }

    public function getPrimaryKey()
    {
        return $this->getId();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getDesc()
    {
        return $this->desc;
    }

    public function addPerson(Person $model)
    {
        $this->persons[] = $model;
    }

    public function getPersons()
    {
        return $this->persons;
    }

    public function getProject(): ?Project
    {
        return $this->project;
    }

    public function getProjectId(): int
    {
        return $this->project_id;
    }

    public function getProjectName(): string
    {
        return $this->project ? $this->getProject()->getName() : '';
    }

    public function getPriority()
    {
        return $this->priority;
    }

    public function getPriorityList()
    {
        return [0,1,2,3,4,5];
    }

}