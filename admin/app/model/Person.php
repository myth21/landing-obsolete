<?php
namespace app\model;

use vendor\myth21\PdoRecord;

class Person extends PdoRecord
{
    protected $id;
    protected $name;
    protected $desc;

    protected $availableAttributes = [
        'name' => 'Name',
        'desc' => 'Desc',
    ];

    public function getTableName()
    {
        return 'person';
    }

    public function getPrimaryKey()
    {
        return $this->getId();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getDesc()
    {
        return $this->desc;
    }

}