<?php
namespace app\model;

use vendor\myth21\PdoRecord;

class PersonProject extends PdoRecord
{
    protected $id;
    protected $person_id;
    protected $project_id;
    protected $desc;

    protected $availableAttributes = [
        'desc' => 'Desc',
    ];

    public function getTableName()
    {
        return 'person_project';
    }

    public function getPrimaryKey()
    {
        return $this->getId();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getDesc()
    {
        return $this->desc;
    }

    public function getPersonId()
    {
        return $this->person_id;
    }

}