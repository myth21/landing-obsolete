<?php
namespace app\model;

use vendor\myth21\PdoRecord;

class Note extends PdoRecord
{
    protected $id;
    protected $title;
    protected $subtitle;
    protected $desc;

    protected $availableAttributes = [
        'title' => 'Title',
        'subtitle' => 'Subtitle',
        'desc' => 'Desc',
    ];

    public function getTableName()
    {
        return 'note';
    }

    public function getPrimaryKey()
    {
        return $this->getId();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function getSubTitle()
    {
        return $this->subtitle;
    }

    public function getDesc()
    {
        return $this->desc;
    }

}