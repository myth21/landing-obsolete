<?php
namespace app\model;

use vendor\myth21\PdoRecord;

class Project extends PdoRecord
{
    protected $id;
    protected $name;
    protected $desc;

    public $persons = [];

    protected $availableAttributes = [
        'name' => 'Name',
        'desc' => 'Desc',
    ];

    public function getTableName()
    {
        return 'project';
    }

    public function getPrimaryKey()
    {
        return $this->getId();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getDesc()
    {
        return $this->desc;
    }

    public function addPerson(Person $model)
    {
        $this->persons[] = $model;
    }

    public function getPersons()
    {
        return $this->persons;
    }

}