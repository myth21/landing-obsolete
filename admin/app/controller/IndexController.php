<?php
namespace app\controller;

use app\model\Note;

class IndexController extends Controller
{
    public function index()
    {
        $this->setView();
        $this->view->setLayout('team');
        $this->view->setTitle('Dashboard');

        $notes = $this->createDbModel(Note::class)->selectList();

        return $this->view->render($this->app->getRequestControllerName() . '/index', [
            'host' => $this->app->getParam('host'),
            'notes' => $notes
        ]);
    }

}
