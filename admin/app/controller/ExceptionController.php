<?php
namespace app\controller;

// LogicException = 500
// BadMethodCallException = 404
// DomainException  = 400, limit input data
// InvalidArgumentException = 400|404, expects type of values or like this ...
class ExceptionController extends Controller
{
    public function handle()
    {
        $code = $this->app->getException()->getCode();

        switch (get_class($this->app->getException())) {
            case \BadMethodCallException::class: $code = 404; break;
        }

        $this->getResponse()->setStatus($code);

        return $this->setView()->render('Exception/index', [
            'exception' => $this->app->getException()
        ]);
    }

}