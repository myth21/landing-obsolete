<?php
namespace app\controller;

use app\model\Person;
use app\model\PersonProject;
use app\model\Project;
use app\component\HttpException;

class ProjectController extends Controller
{
    private function getPrimaryModel($id): Project
    {
        $dbModel = $this->createDbModel(Project::class);
        return $dbModel->selectPrimary($id);
    }

    public function index()
    {
        $projects = $this->createDbModel(Project::class)->selectList();

        $persons = $this->createDbModel(Person::class)->selectList();

        $personProjectModel = $this->createDbModel(PersonProject::class);

        foreach ($projects as $model)
        {
            $personProjects = $personProjectModel->selectList([
                'where' => 'project_id='.$model->getPrimaryKey()
            ]);
            if (!$personProjects) {
                continue;
            }
            foreach ($personProjects as $personProjectModel)
            {
                /** @var PersonProject $personProjectModel */
                $person = $persons[$personProjectModel->getPersonId()];
                $model->addPerson($person);
            }
        }

        $this->setView()->setTitle($this->app->getRequestControllerName().'s');

        return $this->view->render($this->app->getRequestControllerName() . '/list', [
            'model' => $this->createDbModel(Project::class),
            'models' => $projects,
            'app' => $this->app,
        ]);
    }

    public function insert()
    {
        if ($_POST) {
            $model = $this->createDbModel(Project::class);
            $model->init($_POST);
            $model->insert();

            $this->redirect($this->app->getRequestControllerName(), 'index');
        }

        throw new HttpException(400);
    }

    public function edit()
    {
        $model = $this->getPrimaryModel($_GET['id']);

        $this->setView()->setTitle($this->app->getRequestControllerName());

        return $this->view->render($this->app->getRequestControllerName() . '/item', [
            'model' => $model,
            'app' => $this->app,
        ]);
    }

    public function update()
    {
        if ($_POST) {
            $model = $this->getPrimaryModel($_POST['id']);
            $model->init($_POST);
            $model->update();

            $this->redirect($this->app->getRequestControllerName(), 'index');
        }

        throw new HttpException(400);
    }

    public function delete()
    {
        if ($_POST) {
            $model = $this->getPrimaryModel($_POST['id']);
            $model->delete();

            $this->redirect($this->app->getRequestControllerName(), 'index');
        }

        throw new HttpException(400);
    }
}
