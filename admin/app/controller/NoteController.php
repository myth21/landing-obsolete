<?php
namespace app\controller;

use app\model\Note;
use app\component\HttpException;

class NoteController extends Controller
{
    private function getPrimaryModel($id): Note
    {
        $dbModel = $this->createDbModel(Note::class);
        return $dbModel->selectPrimary($id);
    }

    public function index()
    {
        $dbModel = $this->createDbModel(Note::class);
        $models = $dbModel->selectList();

        $this->setView()->setTitle($this->app->getRequestControllerName() . 's');

        return $this->view->render($this->app->getRequestControllerName() . '/list', [
            'model' => $this->createDbModel(Note::class),
            'models' => $models,
            'app' => $this->app,
        ]);
    }

    public function insert()
    {
        if ($_POST) {
            $model = $this->createDbModel(Note::class);
            $model->init($_POST);
            $model->insert();

            $this->redirect($this->app->getRequestControllerName(), 'index');
        }

        throw new HttpException(400);
    }

    public function edit()
    {
        $model = $this->getPrimaryModel($_GET['id']);

        $this->setView()->setTitle($this->app->getRequestControllerName());

        return $this->view->render($this->app->getRequestControllerName(). '/item', [
            'model' => $model,
            'app' => $this->app,
        ]);
    }

    public function update()
    {
        if ($_POST) {
            $model = $this->getPrimaryModel($_POST['id']);
            $model->init($_POST);
            $model->update();

            $this->redirect($this->app->getRequestControllerName(), 'index');
        }

        throw new HttpException(400);
    }

    public function delete()
    {
        if ($_POST) {
            $model = $this->getPrimaryModel($_POST['id']);
            $model->delete();

            $this->redirect($this->app->getRequestControllerName(), 'index');
        }

        throw new HttpException(400);
    }
}
