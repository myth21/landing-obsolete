<?php
namespace app\controller;

use app\model\Person;
use app\model\PersonProject;
use app\model\Project;
use app\model\Task;
use app\component\HttpException;

class TaskController extends Controller
{
    private function getPrimaryModel($id): Task
    {
        $dbModel = $this->createDbModel(Task::class);
        return $dbModel->selectPrimary($id);
    }

    public function index()
    {
        $models = $this->createDbModel(Task::class)->selectList([
            'order' => ' priority DESC'
        ]);
        $projectModel = $this->createDbModel(Project::class);
        foreach ($models as $model)
        {
            $taskProject = $projectModel->selectOne([
                'where' => 'id='.$model->getProjectId()
            ]);
            $model->project = $taskProject;
        }

        $this->setView()->setTitle($this->app->getRequestControllerName().'s');

        return $this->view->render($this->app->getRequestControllerName() . '/list', [
            'model' => $this->createDbModel(Task::class),
            'models' => $models,
            'projects' => $projectModel->selectList(),
            'app' => $this->app,
        ]);
    }

    public function insert()
    {
        if ($_POST) {
            $model = $this->createDbModel(Task::class);
            $model->init($_POST);
            $model->insert();

            $this->redirect($this->app->getRequestControllerName(), 'index');
        }

        throw new HttpException(400);
    }

    public function edit()
    {
        $model = $this->getPrimaryModel($_GET['id']);
        $projectModel = $this->createDbModel(Project::class);

        $this->setView()->setTitle($this->app->getRequestControllerName());

        return $this->view->render($this->app->getRequestControllerName() . '/item', [
            'model' => $model,
            'projects' => $projectModel->selectList(),
            'app' => $this->app,
        ]);
    }

    public function update()
    {
        if ($_POST) {
            $model = $this->getPrimaryModel($_POST['id']);
            $model->init($_POST);
            $model->update();

            $this->redirect($this->app->getRequestControllerName(), 'index');
        }

        throw new HttpException(400);
    }

    public function delete()
    {
        if ($_POST) {
            $model = $this->getPrimaryModel($_POST['id']);
            $model->delete();

            $this->redirect($this->app->getRequestControllerName(), 'index');
        }

        throw new HttpException(400);
    }
}
