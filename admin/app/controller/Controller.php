<?php
namespace app\controller;

use app\model\Advantage;
use app\model\Note;
use app\model\Person;
use app\model\Project;
use app\model\Task;
use vendor\myth21\AppWeb;
use vendor\myth21\PdoRecord;
use vendor\myth21\PdoRecordList;
use vendor\myth21\UrlManager;
use vendor\myth21\View;
use vendor\myth21\WebController;

/**
 * Class Controller
 * is used to be common controller of your app or syntactic sugar
 * @package app\controller
 * @property View $view
 * @property AppWeb $app
 */
class Controller extends WebController
{
    use UrlManager;

    protected $view = null;

    protected function setView(): View
    {
        $this->view = new View();

        $this->view->setViewDir($this->app->getParam('appViewRoot'));
        $this->view->setLayout($this->app->getParam('defaultLayoutName'));
        $this->view->setWebRoot($this->app->getParam('webRoot'));

        $menu = $this->view->renderPart('Widget/menu', [
            'requestControllerName' => $this->app->getRequestControllerName(),
            'items' => [
                'Index',
                (new \ReflectionClass(Person::class))->getShortName(),
                (new \ReflectionClass(Project::class))->getShortName(),
                (new \ReflectionClass(Task::class))->getShortName(),
                (new \ReflectionClass(Note::class))->getShortName(),
            ]
        ]);
        $this->view->addLayoutPart('menu', $menu);

        return $this->view;
    }

    protected function createDbModel($className): PdoRecord
    {
        return new $className($this->app->getParam('dsn'));
    }

    protected function redirect($controller, $action): void
    {
        $this->getResponse()->redirect($this->createUrl($controller, $action));
    }

}
