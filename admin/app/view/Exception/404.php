<?php
/**
 * @var \Throwable $exception
 */
?>
<div class="preFontFamily marginBottom20px"><?php echo $exception->getMessage(); ?></div>
<div class="preFontFamily marginBottom20px"><?php echo $exception->getFile(); ?>:<?php echo $exception->getLine(); ?></div>
<pre><?php echo $exception->getTraceAsString(); ?></pre>
