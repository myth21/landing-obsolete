<div id="modalBg" class="modalBg hidden">
    <div class="modalWindow">
        <button class="modalCloseBtn">X</button>
        <span id="modalMessage" class=""></span>
    </div>
</div>
<script>
function handleModal(message, className) {

    let modalMessage = document.getElementById('modalMessage');
    let modalBg = document.getElementById('modalBg');

    modalMessage.classList.add(className);
    modalMessage.append(message);
    modalBg.classList.remove('hidden');

    modalBg.addEventListener('click', (e) => {
        modalBg.classList.add('hidden');
        modalMessage.textContent = '';
    });
}
</script>
