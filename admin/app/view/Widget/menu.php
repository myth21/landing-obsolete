<?php
/** @var $items array */
/** @var $requestControllerName string */
?>
<div class="menu">
<?php foreach ($items as $item) { ?>
    <a class="menuItem <?php echo ($requestControllerName === $item) ? 'active' : ''; ?>" href="<?php echo $this->createUrl($item, 'index'); ?>">
        <?php echo $item ?>
    </a>
<?php } ?>
</div>
