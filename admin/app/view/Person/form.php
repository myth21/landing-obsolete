<?php
/**
 * @var \vendor\myth21\View $this
 * @var app\model\Person $model
 */
?>
<form class="form" action="<?php echo $this->createUrl($app->getRequestControllerName(), $model->isNew() ? 'insert' : 'update'); ?>" method="post">
    <div class="marginBottom20px">
        <input class="input" type="hidden" name="id" value="<?php echo $model->getId(); ?>">
    </div>
    <div class="marginBottom20px">
        <input class="input" type="text" name="name" value="<?php echo $model->getName(); ?>">
    </div>
    <div class="marginBottom20px">
        <textarea class="input" name="desc"><?php echo $model->getDesc(); ?></textarea>
    </div>
    <div class="marginBottom20px">
        <input class="btn" type="submit" value="Submit">
    </div>
</form>
