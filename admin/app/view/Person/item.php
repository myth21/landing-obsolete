<?php
/**
 * @var \vendor\myth21\View $this
 * @var app\model\Person $model
 */
?>
<h1><?php echo $this->getTitle(); ?></h1>
<?php echo $this->renderPart($app->getRequestControllerName().'/form', [
    'model' => $model,
    'app' => $app,
]); ?>

