<?php
/** @var $this View */
use vendor\myth21\View;
?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo $this->getTitle(); ?></title>
    <?php foreach ($this->getMetaTags() as $name => $content) { ?>
        <meta name="<?php echo $name; ?>" content="<?php echo $content; ?>">
    <?php } ?>
    <link href="<?php echo $this->getWebRoot(); ?>css/grid.css" rel="stylesheet" type="text/css">
    <link href="<?php echo $this->getWebRoot(); ?>css/team.css" rel="stylesheet" type="text/css">
    <link href="<?php echo $this->getWebRoot(); ?>dist/fontawesome-free-5.11.2-web/css/all.min.css" rel="stylesheet" type="text/css">
</head>
<body>
<?php /* ?>
<div class="row" style="border: 1px solid red;">
    <div class="box" style="border: 1px solid green;">
        <div class="nav">
            <div class="col col-3" style="border: 1px solid yellowgreen;">
                <a href="/">Cras eu</a>
            </div>
            <div class="col col-3" style="border: 1px solid yellowgreen;">
                <a href="/">Done</a>
            </div>
            <div class="col col-3" style="border: 1px solid yellowgreen;">
                <a href="/">Tincidunt</a>
            </div>
            <div class="col col-3" style="border: 1px solid yellowgreen;">
                <a href="/">Pellentesque</a>
            </div>
        </div>
    </div>
</div>
<?php */ ?>

<div class="row padding-top padding-bottom">
    <div class="box">
        <div class="col col-4 col-logo">
            <?php /*?><img class="img-responsive" src="<?php echo $this->getWebRoot(); ?>img/_logo.png" style="width: 350px; border: 1px solid red; color: green" alt=""><?php */ ?>
            <div class="col-logo primary1">
                <i class="fas fa-user-friends fa-10x"></i>
                <div class="col-logo-text">WDT</div>
            </div>
        </div>
        <div class="col col-1">
            &nbsp;
        </div>
        <div class="col col-7">
            <div class="">
                <h1 class="margin-bottom ">Web Development Team</h1>
                <div class="quote margin-bottom">
                    Bill Gates Quote: "If your business is not on the Internet, then your business will be out of business."<br>
                    <!--https://gravitatewebdesign.com/blog/if-your-business-is-not-on-the-internet-then-your-business-will-be-out-of-business/-->
                    We: "We will do our best to help the business."
                </div>
                <div class="_margin-bottom">
                    We develop websites and tools to solve your problems in the context of the world wide web.
                    Our technologies are simple and reliable which will allow you to be effective on the Internet.
                </div>
                <?php /* ?>
                    As low as $25 per hour!
                    Please contact us. No obligations.
                <?php */ ?>
            </div>
        </div>
    </div>
</div>

<div class="row padding-top padding-bottom team">
    <div class="box">
        <div class="col col-12">
            <h2 class=" margin-bottom">Team</h2>

            <div class="col col-4 margin-bottom">
                <div class="team-item small-margin-bottom">
                    <img class="border-radius-50per img-responsive" src="<?php echo $this->getWebRoot(); ?>img/michael.jpg" alt="">
                </div>
                <h3 class="small-margin-bottom">UX Designer</h3>
                <div>ux designer, web developer I Seo
                    Michael thinks out the most convenient solutions for presenting information.
                    Creates the web (or mobile) interface clear and easy to use.
                </div>
            </div>

            <div class="col col-4 margin-bottom">
                <div class="team-item small-margin-bottom">
                    <img class="border-radius-50per img-responsive" src="<?php echo $this->getWebRoot(); ?>img/ivan.jpg" alt="">
                </div>
                <h3 class="small-margin-bottom">Web Developer</h3>
                <div>
                    Ivan manages project, develops the website architecture and selects the right tools to implement it  based on customer requirements.
                    Provides reliable website work.
                </div>
            </div>

            <div class="col col-4 margin-bottom">
                <div class="team-item small-margin-bottom">
                    <img class="border-radius-50per img-responsive" src="<?php echo $this->getWebRoot(); ?>img/rita.jpg" alt="">
                </div>
                <h3 class="small-margin-bottom">SEO specialist</h3>
                <div>
                    Margaret analyzes, reviews and implements content (text, images, video, sound and so on) changes to websites so they are optimized for search engines.
                </div>
            </div>

        </div>
    </div>
</div>

<div class="row padding-top padding-bottom bg-primary1">
    <div class="box">

        <div class="col col-12 invert-color">
            <h2 class="margin-bottom invert-color">Specialization</h2>
            <div class="col col-3 margin-bottom">
                <div class="fa-item small-margin-bottom">
                    <span class="fa-stack fa-4x">
                        <?php /* ?>
                        <i class="fa fa-circle fa-stack-2x"></i>
                        <i class="fa fa-lightbulb fa-stack-1x primary1"></i>
                        <?php */ ?>
                        <i class="fas fa-layer-group"></i>
                    </span>
                </div>
                <h3 class="small-margin-bottom invert-color">Web Design Services</h3>
                <div>
                    Creating a website and mobile app design.
                    Development of individual photo collages, illustrations for sites and unique icons.
                </div>
            </div>
            <div class="col col-3 margin-bottom">
                <div class="fa-item small-margin-bottom">
                    <span class="fa-stack fa-4x">
                        <i class="fas fa-file-code"></i>
                    </span>
                </div>
                <h3 class="small-margin-bottom invert-color">Website development</h3>
                <div>
                    Developing landing pages, company information websites, catalogs, and other website types by agreement.
                </div>
            </div>
            <div class="col col-3 margin-bottom">
                <div class="fa-item small-margin-bottom">
                    <span class="fa-stack fa-4x">
                        <i class="fas fa-headset"></i>
                    </span>
                </div>
                <h3 class="small-margin-bottom invert-color">Website tech support</h3>
                <div>
                    Website content changing, adding new features, and improving current features.
                </div>
            </div>
            <div class="col col-3 margin-bottom">
                <div class="fa-item small-margin-bottom">
                    <span class="fa-stack fa-4x">
                        <i class="fas fa-search-dollar"></i>
                    </span>
                </div>
                <h3 class="small-margin-bottom invert-color">Search Engine Optimization</h3>
                <div class="">
                    Increasing the visibility of a website or a web page to users of a web search engine.
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row padding-top padding-bottom">
    <div class="box">
        <div class="col col-12">
            <h2 class="margin-bottom">Portfolio</h2>

            <div class="portfolio-items">
                <div class="col-5 portfolio-item">
                    <?php // http://ami.responsivedesign.is ?>
                    <img class="img-responsive" src="<?php echo $this->getWebRoot(); ?>img/pvd.png" alt="image">
                </div>
                <div class="col-1 portfolio-item">&nbsp;</div>
                <div class="col-6 portfolio-item">
                    <h3 class="head-text-align small-margin-bottom">HimSibTorg Ltd.</h3>
                    <div class="small-margin-bottom">
                        We collected and analyzed the client's requirements, and then developed a prototype of the site.
                        <br>
                        <br>
                        After that, we created the design concept of the page and defined the design style, selected the color scheme and fonts.
                        <br>
                        <br>
                        Finally, the website page and selected the content was created that is interesting for business customers.
                    </div>
                    <div class="btn-align">
                        <a class="btn bg-complement2 invert-color" href="//himsibtorg.ru" target="_blank">Link</a>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>


<div class="row bg-primary1 padding-top padding-bottom footer">
    <div class="box">
        <div class="col col-12">

            <h2 class=" margin-bottom invert-color">Contact</h2>

            <div class="small-margin-bottom">
                Office in New York, USA
            </div>
            <div class="small-margin-bottom">
                Development centre in Tomsk, Russia
            </div>
            <div class="margin-bottom">
                <a href="mailto:www.to.business+hi@gmail.com" class="invert-color">www.to.business+hi@gmail.com</a>
            </div>
            <?php /* ?>
            <div class="small-margin-bottom">
                <a href="tel:79539210007">+7-953-921-00-07</a>
            </div>
            <div class="">
                <a class="btn whatsAppBgColor invert-color" href="https://wa.me/79539210007" target="_blank">WhatsApp</a>&nbsp;
                <a class="btn telegramBackgroundColor invert-color" href="tg://resolve?domain=WebDeveloperGroup" target="_blank">Telegram</a>&nbsp
                <a class="btn skypeBackgroundColor invert-color" href="skype:live:.cid.b53ad35f7209c221" target="_blank">Skype</a>
            </div>
            <?php */ ?>

            <?php /* ?>
            <form class="margin-bottom" method="post" id="contactForm" action="/" style="width: 270px; margin: 0 auto">
                <fieldset>
                    <legend> Send Message </legend>
                    <input type="hidden" name="contactToken" value="5a870e01d91ca91281cc5377f2394075">
                    <div class="marginBottom20px">
                        <input type="email" class="input" id="user_email" name="user_email" placeholder="Email" required="">
                    </div>
                    <div class="marginBottom20px">
                        <textarea class="input" name="user_message" id="user_message" placeholder="Message" required=""></textarea>
                    </div>
                    <div>
                        <button type="submit" class="btn _bgSecondary1 " id="contactFormSubmit">Pretium</button>
                    </div>
                </fieldset>
            </form>
            <div id="modalBg" class="modalBg hidden">
                <div class="modalWindow">
                    <button class="modalCloseBtn">X</button>
                    <span id="modalMessage" class=""></span>
                </div>
            </div>
            <script>
                function handleModal(message, className) {

                    let modalMessage = document.getElementById('modalMessage');
                    let modalBg = document.getElementById('modalBg');

                    modalMessage.classList.add(className);
                    modalMessage.append(message);
                    modalBg.classList.remove('hidden');

                    modalBg.addEventListener('click', (e) => {
                        modalBg.classList.add('hidden');
                        modalMessage.textContent = '';
                    });
                }
            </script>
            <script>
                document.addEventListener('DOMContentLoaded', () => {
                    setFormHandler('contactForm');
                });
            </script>
            <?php */ ?>

        </div>
    </div>
</div>

</body>
</html>