<?php
/** @var $this View */
use vendor\myth21\View;
?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo $this->getTitle(); ?></title>
    <?php foreach ($this->getMetaTags() as $name => $content) { ?>
        <meta name="<?php echo $name; ?>" content="<?php echo $content; ?>">
    <?php } ?>
    <link href="<?php echo $this->getWebRoot(); ?>css/grid.css" rel="stylesheet" type="text/css">
    <link href="<?php echo $this->getWebRoot(); ?>css/app.css" rel="stylesheet" type="text/css">
</head>
<body>
<div class="box">
    <div class="col col-2">
        <?php echo $this->getLayoutPart('menu'); ?>
    </div>
    <div class="col col-1">&nbsp;</div>
    <div class="col col-9">
        <?php echo $this->getContent(); ?>
    </div>
</div>
<script>
    document.addEventListener('DOMContentLoaded', () => {
        var textarea = document.querySelector('textarea');
        if (textarea) {
            function resizeTextarea(textarea) {
                textarea.style.height = 'auto';
                textarea.style.height = textarea.scrollHeight + 2 + 'px';
            }
            resizeTextarea(textarea);
            //textarea.addEventListener('input', function (e) { resizeTextarea(e.target) });
        }
    });
</script>
</body>
</html>