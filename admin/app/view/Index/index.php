<?php
/** @var \vendor\myth21\View $this */
/** @var string $host */
/** @var array $notes */
?>
<h1 class="marginBottom20px"><?php echo $this->getTitle(); ?></h1>

<?php foreach ($notes as $note) { ?>
    <div class="marginBottom20px">
        <?php echo nl2br($note->getDesc()); ?>
    </div>
<?php } ?>
