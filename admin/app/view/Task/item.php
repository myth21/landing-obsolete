<?php
/**
 * @var \vendor\myth21\View $this
 * @var app\model\Task $model
 * @var app\model\Project[] $projects
 */
?>
<h1><?php echo $this->getTitle(); ?></h1>
<?php echo $this->renderPart($app->getRequestControllerName().'/form', [
    'model' => $model,
    'projects' => $projects,
    'app' => $app,
]); ?>

