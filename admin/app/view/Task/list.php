<?php
/**
 * @var \vendor\myth21\View $this
 * @var \vendor\myth21\AppWeb $app
 * @var app\model\Task $model
 * @var app\model\Task[] $models
 * @var app\model\Project[] $projects
 */
?>
<h1 class="marginBottom20px"><?php echo $this->getTitle(); ?></h1>

<?php echo $this->renderPart($app->getRequestControllerName().'/form', [
        'model' => $model,
        'projects' => $projects,
        'app' => $app,
]); ?>

<table class="table">
    <thead>
        <tr>
            <th><?php echo $model->getLabel('project'); ?></th>
            <th><?php echo $model->getLabel('name'); ?></th>
            <th><?php echo $model->getLabel('desc'); ?></th>
            <th><?php echo $model->getLabel('priority'); ?></th>
            <th>&nbsp;</th>
            <th>&nbsp;</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($models as $model) { ?>
            <tr>
                <td data-label="<?php echo $model->getLabel('project'); ?>"><?php echo $model->getProjectName(); ?></td>
                <td data-label="<?php echo $model->getLabel('name'); ?>"><?php echo $model->getName(); ?></td>
                <td data-label="<?php echo $model->getLabel('desc'); ?>"><?php echo nl2br($model->getDesc()); ?></td>
                <td data-label="<?php echo $model->getLabel('priority'); ?>"><?php echo $model->getPriority(); ?></td>
                <td>
                    <a href="<?php echo $this->createUrl($app->getRequestControllerName(), 'edit', ['id' => $model->getId()]); ?>">Edit</a>
                </td>
                <td>
                    <form action="<?php echo $this->createUrl($app->getRequestControllerName(), 'delete', ['id' => $model->getId()]); ?>" method="post">
                        <input class="input" type="hidden" name="id" value="<?php echo $model->getId(); ?>">
                        <input class="cursorPointer" type="submit" value="Del" onclick="return confirm('Delete?')">
                    </form>
                </td>
            </tr>
        <?php } ?>
    </tbody>
</table>