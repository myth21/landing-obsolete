<?php
/**
 * @var \vendor\myth21\View $this
 * @var app\model\Task $model
 * @var app\model\Project[] $projects
 */
?>
<form class="form" action="<?php echo $this->createUrl($app->getRequestControllerName(), $model->isNew() ? 'insert' : 'update'); ?>" method="post">
    <div class="marginBottom20px">
        <input class="input" type="hidden" name="id" value="<?php echo $model->getId(); ?>">
    </div>
    <div class="marginBottom20px">
        <select class="input" name="project_id">
            <?php foreach ($projects as $project) { ?>
                <?php // TODO move out $model->getProjectId() == $project->getId() ?>
                <option <?php echo $model->getProjectId() == $project->getId() ? 'selected' : ''; ?>  value="<?php echo $project->getId(); ?>"><?php echo $project->getName(); ?></option>
            <?php } ?>
        </select>
    </div>
    <div class="marginBottom20px">
        <input class="input" type="text" name="name" value="<?php echo $model->getName(); ?>">
    </div>
    <div class="marginBottom20px">
        <textarea class="input" name="desc"><?php echo $model->getDesc(); ?></textarea>
    </div>
    <div class="marginBottom20px">
        <select class="input" name="priority">
            <?php foreach ($model->getPriorityList() as $priority) { ?>
                <option <?php echo $model->getPriority() == $priority ? 'selected' : ''; ?> value="<?php echo $priority; ?>"><?php echo $priority; ?></option>
            <?php } ?>
        </select>
    </div>
    <div class="marginBottom20px">
        <input class="btn" type="submit" value="Submit">
    </div>
</form>
