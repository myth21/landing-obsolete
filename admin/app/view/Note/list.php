<?php
/**
 * @var \vendor\myth21\View $this
 * @var app\model\Note $model
 * @var app\model\Note[] $models
 */
?>
<h1 class="marginBottom20px"><?php echo $this->getTitle(); ?></h1>

<?php echo $this->renderPart($app->getRequestControllerName().'/form', [
    'model' => $model,
    'app' => $app,
]); ?>

<table class="table">
    <thead>
        <tr>
            <th><?php echo $model->getLabel('title'); ?></th>
            <th><?php echo $model->getLabel('subtitle'); ?></th>
            <th><?php echo $model->getLabel('desc'); ?></th>
            <th>&nbsp;</th>
            <th>&nbsp;</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($models as $model) { ?>
            <tr>
                <td data-label="<?php echo $model->getLabel('title'); ?>"><?php echo $model->getTitle(); ?></td>
                <td data-label="<?php echo $model->getLabel('subtitle'); ?>"><?php echo $model->getSubTitle(); ?></td>
                <td data-label="<?php echo $model->getLabel('desc'); ?>"><?php echo nl2br($model->getDesc()); ?></td>
                <td >
                    <a href="<?php echo $this->createUrl($app->getRequestControllerName(), 'edit', ['id' => $model->getId()]); ?>">Edit</a>
                </td>
                <td>
                    <form action="<?php echo $this->createUrl($app->getRequestControllerName(), 'delete', ['id' => $model->getId()]); ?>" method="post">
                        <input class="input" type="hidden" name="id" value="<?php echo $model->getId(); ?>">
                        <input class="cursorPointer" type="submit" value="Del" onclick="return confirm('Delete?')">
                    </form>
                </td>
            </tr>
        <?php } ?>
    </tbody>
</table>