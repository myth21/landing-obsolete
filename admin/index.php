<?php
$params = require_once 'params.php';
$localParams = require_once 'params-local.php';
$params = array_merge($params, $localParams);

require_once 'error-handler.php';

spl_autoload_register(function ($className) {
    $fileName = str_replace("\\", DIRECTORY_SEPARATOR, $className);
    if (!file_exists($fileName . '.php')) {
        throw new \BadMethodCallException('Class not found');
    }
    require_once $fileName . '.php';
});

$app = vendor\myth21\App::factory($params);
$app->run();