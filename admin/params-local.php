<?php

return [
    'host' => 'example.loc',
    'webRoot' => '/admin/',
    'dsn' => 'sqlite:' . __DIR__ . DIRECTORY_SEPARATOR . '.db',
    'adminEmail' => 'ivan.malukhin@gmail.com',
    'displayErrors' => true,
];