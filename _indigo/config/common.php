<?php

ini_set('error_reporting', E_ALL);
ini_set('display_errors', 0);
ini_set('display_startup_errors', 0);

register_shutdown_function('\\indigo\\ErrorHandler::logShutDown');
set_error_handler( "\\indigo\\ErrorHandler::logError" );
set_exception_handler( "\\indigo\\ErrorHandler::logException" );

define('EXT', '.php');
define('ENTRY_POINT', 'index');
define('DS', DIRECTORY_SEPARATOR);
define('DOC_ROOT', \indigo\Server::getInstance()->getDocRoot());

date_default_timezone_set('Asia/Tomsk');

return [
    'developmentEnv' => true,
    'dir' => [
        'app' => DOC_ROOT . 'application' . DS,
        'config' => DOC_ROOT . 'config' . DS,
        'language' => DOC_ROOT . 'language' . DS,
        'module' => DOC_ROOT . 'module' . DS,
        'model' => DOC_ROOT . 'application' . DS,
        'storage' => DOC_ROOT . 'storage' . DS,
        'component' => DOC_ROOT . 'application' . DS . 'component' . DS,
        'console' => DOC_ROOT . 'application' . DS . 'console' . DS,
        'controller' => DOC_ROOT . 'application' . DS . 'controller' . DS,
        'view' => DOC_ROOT . 'application' . DS . 'view' . DS,

        'web' => DOC_ROOT . 'web' . DS,
        'css' => DOC_ROOT . 'web' . DS . 'css' . DS,
        'img' => DOC_ROOT . 'web' . DS . 'img' . DS,
        'design' => DOC_ROOT . 'web' . DS . 'img' . DS . 'design' . DS,
        'js' => DOC_ROOT . 'web' . DS . 'js' . DS,
    ],
    'app' => [

        'css' => 'web/css/',
        'img' => 'web/img/',
        'design' => 'web/img/design/',
        'js' => 'web/js/',

        'title' => 'Simple Framework Indigo',
        'defaultTemplate' => 'template_default',
        'defaultLanguage' => 'ru-RU',
        'dateFormat' => 'Y-m-d',
        
        'errorLogFile' => DOC_ROOT . 'storage' . DS . 'errors.log',
    ],
    'secure' => [
        'salt' => 'slt',
    ],
    'sqlite' => [
        'dsn' => 'sqlite:' . DOC_ROOT . 'storage' . DS. 'indigo.sqlite',
        'driver' => 'sqlite',
        'host' => 'sqlite',
        'name' => 'indigo.sqlite',
        'userName' => '',
        'password' => 'passwd',
        'options' => [],
    ],
    'defaultStorage' => 'sqlite',
    'admin' => [
        'login' => 'login',
        'password' => 'password',
        'email' => 'admin@admin.com',
        'name' => 'Admin Name',
    ],

];