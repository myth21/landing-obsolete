<?php

// autoload_namespaces.php @generated by Composer

$vendorDir = dirname(dirname(__FILE__));
$baseDir = dirname($vendorDir);

return array(
    'View' => array($baseDir . '/application/view'),
    'Model' => array($baseDir . '/application/model'),
    'Controller' => array($baseDir . '/application/controller'),
    'Console' => array($baseDir . '/application/console'),
);
