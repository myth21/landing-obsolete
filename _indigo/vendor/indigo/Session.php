<?php

namespace indigo;

class Session
{

    private static $_instance;

    public $_data = [];

    private function init()
    {
        session_start();
        $this->_data =& $_SESSION;
    }

    static public function getInstance()
    {
        if (is_null(self::$_instance)) {
            self::$_instance = new self();
            self::$_instance->init();
        }
        return self::$_instance;
    }

    public function set($key, $value)
    {
        $this->_data[$key] = $value;
    }

    public function has($id)
    {
        return isset($this->_data[$id]) ? true : false;
    }

    public function get($key, $delete = false)
    {
        $keyExists = array_key_exists($key, $this->_data);

        if ($keyExists && $delete) {
            $data = $this->_data[$key];
            $this->delete($key);
            return $data;
        }

        if ($keyExists) {
            return $this->_data[$key];
        }

        return null;
    }

    public function delete($key)
    {
        unset($this->_data[$key]);
    }

    public function destroySession()
    {
        return session_destroy();
    }

}