<?php

namespace indigo;

class View
{
    private static $_instance;

    protected $viewDir = '';
    protected $defaultTemplate = '';
    protected $queryController = '';
    protected $title = '';
    protected $templateData = [];
    protected $content = [];
    protected $partContent = [];

    /** @var $config \indigo\Config */
    protected $config = null;

    private function init()
    {
        $this->config = Config::getInstance();
        $this->viewDir = $this->config->getParam('dir.view');
        $this->defaultTemplate = $this->config->getParam('app.defaultTemplate');
    }

    static public function getInstance()
    {
        if (self::$_instance === null) {
            self::$_instance = new self();
            self::$_instance->init();
        }

        return self::$_instance;
    }

    public function setToTemplate(array $data)
    {
        foreach ($data as $templateKey => $templateValue) {
            $this->templateData[$templateKey] = $templateValue;
        }

        return $this->templateData;
    }

    public function addData(array $data, $partialName = '')
    {
        foreach ($data as $key => $value) {
            $this->partContent[$partialName][$key] = $value;
        }

        return $this->partContent[$partialName];
    }

    public function isData($partialName = '')
    {
        return isset($this->partContent[$partialName]) ? true : false;
    }

    public function getData($partialName = '')
    {
        return $this->partContent[$partialName];
    }

    public function setTitle($title)
    {
        $this->title = $title;
    }

    public function getTitle($appTitle = true)
    {
        if ($appTitle) {
            return $this->title . ' | ' . $this->config->getParam('app.title');
        }

        return $this->title;
    }

    public function getContent()
    {
        return $this->content;
    }

    public function isContent()
    {
        return $this->content ? true : false;
    }

    public function setContent($contentView, array $data)
    {
        $pathToViewFile = $this->viewDir . $contentView . EXT;

        if (is_readable($pathToViewFile)) {
            ob_start();
            extract($data);
            require_once $pathToViewFile;
            $this->content[] = ob_get_clean();

            return $this->content;
        }

        throw new \ErrorException('File ' . $pathToViewFile . ' not found');
    }

    // TODO: to implement html parts

    public function render($template = null)
    {
        $template = $template ? $template : $this->defaultTemplate;
        $pathToViewFile = $this->viewDir . $template . EXT;

        if (is_readable($pathToViewFile)) {
            ob_start();
            ob_implicit_flush(false);
            extract($this->templateData, EXTR_OVERWRITE);
            require_once($pathToViewFile);
            $output = ob_get_clean();

            return $output;
        }

        throw new \ErrorException('File ' . $pathToViewFile . ' not found');
    }

    public function setQueryController($queryController)
    {
        $this->queryController = $queryController;
    }

    public function getQueryController()
    {
        return $this->queryController;
    }

    public function getLinkClassName($menuKey, $defaultName = 'active')
    {
        if ($this->getQueryController() == $menuKey) {
            return $defaultName;
        }

        return '';
    }

}