<?php

namespace indigo;

class Cookie
{

    static public function set($key, $value = null, $expire = 0, $path = '/', $domain = '', $secure = null, $httpOnly = true, $md5 = false)
    {
        if ($md5) {
            $value = md5($value . Config::getInstance()->getParam('secure.salt'));
        }

        if (is_null($secure)) {
            $secure = Server::getInstance()->isHttpsProtocol() ? true : false;
        }

        return setcookie($key, $value, $expire, $path, $domain, $secure, $httpOnly);
    }

    static public function get($key)
    {
        return array_key_exists($key, $_COOKIE) ? $_COOKIE[$key] : null;
    }

    static public function delete($key)
    {
        return setcookie($key, null, -86400);
    }

}