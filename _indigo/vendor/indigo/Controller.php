<?php

namespace indigo;

abstract class Controller
{
    protected $request = null;
    protected $response = null;

    private $name = null;
    private $actionName = null;

    abstract public function before();

    abstract public function actionIndex();

    public function __construct()
    {
        $this->request = Request::getInstance();
        $this->response = Response::getInstance();
    }

    public function execute()
    {
        $this->before();
        $methodName = $this->getMethodName();
        $body = $this->$methodName();
        $this->response->setBody($body);

        return $this->response;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function setActionName($actionName)
    {
        $this->actionName = $actionName;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getActionName()
    {
        return $this->actionName;
    }

    public function getMethodName()
    {
        return Router::ACTION . ucfirst($this->getActionName());
    }
}

?>