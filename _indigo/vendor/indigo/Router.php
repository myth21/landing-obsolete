<?php

namespace indigo;

class Router
{
    const CONSOLE = 'Console';
    const CONTROLLER = 'Controller';
    const INDEX = 'Index';
    const EXCEPTION = 'Exception';
    const ACTION = 'action';

    private static $_instance;

    /**
     * @var $_server Server
     */
    private $_server = null;

    /**
     * @var $_request Request
     */
    private $_request = null;

    /**
     * @var $_response Response
     */
    private $_response = null;

    /**
     * @var $_controller Controller
     */
    private $_controller = null;

    private $_controllerName = null;
    private $_className = null;
    private $_actionName = null;

    private function init()
    {
        $this->_server = Server::getInstance();
        $this->_request = Request::getInstance();

        if ($this->_server->isHttpRequest()) {
            $this->_response = Response::getInstance();
        }
    }

    static public function getInstance()
    {
        if (self::$_instance === null) {
            self::$_instance = new self();
            self::$_instance->init();
        }
        return self::$_instance;
    }

    public function getController()
    {
        return $this->_controller;
    }

    public function defineRoute()
    {
        $this->_controllerName = $this->_request->getParam(lcfirst(self::CONTROLLER));
        $this->_className = $this->getClassName();

        if ($this->_className == self::CONTROLLER . self::EXCEPTION) {
            $this->_actionName = $this->_response->getStatusCode();
        } else {
            $this->_actionName = $this->_request->isParam(self::ACTION) ? $this->_request->getParam(self::ACTION) : self::INDEX;
            if ($this->getMethodName() == self::ACTION . self::EXCEPTION) {
                $this->_className = self::CONTROLLER . self::EXCEPTION;
                $this->_actionName = $this->_response->getStatusCode();
            }
        }

        $this->setExecutor();

        return $this;
    }


    /**
     * Input console interface: controllerName/actionName
     * @return $this
     */
    public function defineScript()
    {
        $params = $this->_request->getParams();
        if (!isset($params[1]) || !Console::isPatternCommand($params[1])) {
            Console::exitScript('ConsoleScript not match with a pattern');
        }

        $controllerAndAction = explode('/', $params[1]);
        $this->_controllerName = $controllerAndAction[0];
        $this->_actionName = $controllerAndAction[1];

        $this->_className = $this->getClassName();
        if ($this->_className == self::CONSOLE . self::EXCEPTION) {
            $this->_actionName = self::EXCEPTION;
        } else {
            if ($this->getMethodName() == self::ACTION . self::EXCEPTION) {
                $this->_className = self::CONSOLE . self::EXCEPTION;
                $this->_actionName = self::EXCEPTION;
            }
        }

        $this->setExecutor();

        return $this;
    }

    private function getClassName()
    {
        if ($this->_server->isHttpRequest()) {
            if ($this->_controllerName) {
                if (class_exists(self::CONTROLLER . ucfirst($this->_controllerName))) {
                    return self::CONTROLLER . ucfirst($this->_controllerName);
                } else {
                    $this->_response->setStatusCode(404, 'Class ' . $this->_controllerName . ' not found');
                    return self::CONTROLLER . self::EXCEPTION;
                }
            } else {
                return self::CONTROLLER . self::INDEX;
            }
        }

        if ($this->_controllerName && class_exists(self::CONSOLE . ucfirst($this->_controllerName))) {
            return self::CONSOLE . ucfirst($this->_controllerName);
        }

        return self::CONSOLE . self::EXCEPTION;
    }

    private function getMethodName()
    {
        $methodName = self::ACTION . ucfirst($this->_actionName);
        if ($this->_actionName) {
            $reflectionClass = new \ReflectionClass($this->_className);
            if ($reflectionClass->hasMethod($methodName)) {
                return $methodName;
            } else {
                if ($this->_server->isHttpRequest()) {
                    $this->_response->setStatusCode(404, 'Method ' . $methodName . ' not found');
                    return self::ACTION . self::EXCEPTION;
                } else {
                    Console::exitScript('Action ' . $methodName . ' not found');
                }
            }
        }

        return self::ACTION . self::INDEX;
    }

    protected function setExecutor()
    {
        $this->_controller = new $this->_className();
        $this->_controller->setName($this->_controllerName);
        $this->_controller->setActionName($this->_actionName);
        return $this->_controller;
    }

    public function getUrl($controller = null, $action = null, $params = [])
    {
        if ($controller == null && $action == null && $params == []) {
            return $this->_request->getUrl();
        }

        // TODO: to improve the solution, i.e. /controller/action/key1/value1/key2/value2... or another
        $url = ENTRY_POINT . EXT . '?';
        if ($controller) {
            $url .= 'controller=' . $controller;
            if ($action) {
                $url .= '&action=' . $action;
                if ($params) {
                    foreach ($params as $key => $value) {
                        $url .= '&' . $key . '=' . $value;
                    }
                }
            }
        }

        return $url;
    }
}