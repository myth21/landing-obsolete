<?php

namespace indigo;

class Language
{

    const LANG_FILE_EXT = '.ini';

    const RU_RU = 'ru-RU';
    const EN_GB = 'en-GB';

    static public function _($key)
    {
        $config = Config::getInstance();
        $userLanguage = Cookie::get('language');

        if (is_null($userLanguage)) {
            $pathToLanguageFile = $config->getParam('dir.language') . $config->getParam('app.defaultLanguage') . self::LANG_FILE_EXT;
        } else {
            $pathToLanguageFile = $config->getParam('dir.language') . $userLanguage . self::LANG_FILE_EXT;
        }

        if (is_readable($pathToLanguageFile)) {
            $iniData = parse_ini_file($pathToLanguageFile);
            if (isset($iniData[$key])) {
                return $iniData[$key];
            }

            throw new \ErrorException('Language value not found');
        }

        throw new \ErrorException('Language file ' . $pathToLanguageFile . ' not found.');
    }

}