<?php

namespace indigo;

/**
 * Server
 *
 * Local server environment
 *
 * @version 1.0
 */
class Server
{
    /**
     * @var object one class instance
     */
    private static $_instance;

    private $_isHttpRequest;
    private $_host;
    private $_protocol;
    private $_protocolVersion;
    private $docRoot;

    private function __construct()
    {
    }

    private function __clone()
    {
    }

    private function __wakeup()
    {
    }

    private function init()
    {
        if (isset($_SERVER['REQUEST_METHOD'])) { // http interface

            $this->_isHttpRequest = true;
            $this->setHost();
            $this->setProtocol();
            $this->setProtocolVersion();

        } elseif (isset($_SERVER['argv'])) { // console interface, get params

            $this->_isHttpRequest = false;

        } else {

            throw new \ErrorException('Request is not defined.');
        }

        $this->setDocRoot();

        return $this;
    }

    public function isHttpRequest()
    {
        return $this->_isHttpRequest;
    }

    static public function getInstance()
    {
        if (self::$_instance === null) {
            self::$_instance = new self();
            self::$_instance->init();
        }
        return self::$_instance;
    }

    public function setHost()
    {
        $this->_host = $_SERVER['HTTP_HOST'] . '/';
    }

    public function setProtocol()
    {
        $protocol = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443)
            ? "https://"
            : "http://";
        $this->_protocol = $protocol;
    }

    public function isHttpsProtocol()
    {
        return (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443)
            ? true
            : false;
    }

    public function getProtocol()
    {
        return $this->_protocol;
    }

    public function getProtocolHost()
    {
        return $this->getProtocol() . $this->getHost();
    }

    public function getHost()
    {
        return $this->_host;
    }

    private function setDocRoot()
    {
        if ($this->isHttpRequest()) {
            $this->docRoot = $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR;
        } else {
            //echo PHP_EOL;
            //print_r(realpath(dirname($_SERVER['argv'][0])));
            //echo PHP_EOL;
            //die;

            $indexDirectory = realpath(dirname($_SERVER['argv'][0]));
            chdir($indexDirectory);

            $this->docRoot = $indexDirectory . DIRECTORY_SEPARATOR;
        }

        $this->docRoot = $this->docRoot . 'indigo' . DIRECTORY_SEPARATOR;
    }

    public function getDocRoot()
    {
        return $this->docRoot;
    }

    public function setProtocolVersion()
    {
        $protocolVersion = explode('/', $_SERVER['SERVER_PROTOCOL']);
        $this->_protocolVersion = $protocolVersion[1];
    }

    public function getProtocolVersion()
    {
        return $this->_protocolVersion;
    }

}
