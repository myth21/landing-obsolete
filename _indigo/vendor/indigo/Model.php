<?php
namespace indigo;

abstract class Model
{

    /**
     * @var object default storage
     */
    protected $storage;

    static public function factory($name)
    {
        $className = self::getCalledClassName($name);
        if (class_exists($className)) {
            $model = new $className();
            $model->initStorage();
            return $model;
        }

        throw new \ErrorException('Class ' . $className . ' not found');
    }

    protected function initStorage()
    {
        $defaultStorage = Config::getInstance()->getParam('defaultStorage');
        $defaultStorageName = 'get' . $defaultStorage . 'Instance';
        $this->storage = Storage::$defaultStorageName();
    }

    private static function getCalledClassName($name)
    {
        return get_called_class() . ucfirst($name);
    }

}

?>