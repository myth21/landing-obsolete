<?php

namespace indigo;

abstract class Console
{
    protected $output = '';

    private $_name = null;
    private $_actionName = null;

    public function execute()
    {
        $methodName = $this->getMethodName();
        $this->$methodName();
        return $this->output;
    }

    public function setName($name)
    {
        $this->_name = $name;
    }

    public function setActionName($actionName)
    {
        $this->_actionName = $actionName;
    }

    public function getActionName()
    {
        return $this->_actionName;
    }

    public function getMethodName()
    {
        return Router::ACTION . ucfirst($this->getActionName());
    }

    static public function isPatternCommand($string)
    {
        return preg_match('/\w\/\w/', $string);
    }

    static public function exitScript($message)
    {
        exit($message);
    }
}