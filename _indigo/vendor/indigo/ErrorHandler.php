<?php

namespace indigo;

class ErrorHandler
{

    static public function logShutDown()
    {
        $error = error_get_last();
        if ($error['type'] == E_ERROR) {
            self::logError($error['type'], $error['message'], $error['file'], $error['line']);
        }
    }

    static public function logError($num, $str, $file, $line)
    {
        self::logException(new \ErrorException($str, 0, $num, $file, $line));
    }

    // Not caught exception
    static public function logException(\Throwable $e)
    {
        $config = Config::getInstance();
        $response = Response::getInstance();

        $output = $e->getMessage() . '; ' . $e->getFile() . ': ' . $e->getLine();

        if (Server::getInstance()->isHttpRequest()) {

            // TODO: template for 500 error
            $obSize = ob_get_length();
            if ($obSize) {
                ob_clean();
            }

            if ($config->getParam('developmentEnv')) {

                echo $output;

            } else {

                if (is_writable($config->getParam('app.errorLogFile'))) {
                    file_put_contents($config->getParam('app.errorLogFile'), $output . PHP_EOL, FILE_APPEND);
                }

                echo Language::_('ERROR_500_MESSAGE');
            }

            $response->setStatusCode(500, $output);
            $response->sendHeaders();

        }

        exit();
    }

}