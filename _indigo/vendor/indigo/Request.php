<?php

namespace indigo;

class Request
{

    private $params = [];

    private static $_instance;


    static public function getInstance()
    {
        if (self::$_instance === null) {
            self::$_instance = new self();
            self::$_instance->setParams();
        }

        return self::$_instance;
    }


    private function setParams()
    {
        if (Server::getInstance()->isHttpRequest()) // http interface
        {
            // var constrains...
            foreach ($_SERVER as $key => $value) {
                $this->setParam($key, $value);
            }
        } else // console interface, get params
        {
            foreach ($_SERVER['argv'] as $key => $value) {
                $this->setParam($key, $value);
            }
        }
    }


    private function setParam($key, $value)
    {
        $this->params[$key] = $value;
    }


    public function isParam($key)
    {
        return (isset($this->params[$key]) || $this->isQueryParam($key)) ? true : false;
    }


    public function isQueryParam($key)
    {
        return (bool)$this->getQueryParam($key);
    }


    public function getParam($key)
    {
        if (isset($this->params[$key])) {
            return $this->params[$key];
        } elseif ($this->isQueryParam($key)) {
            return $this->getQueryParam($key);
        }

        return null;
    }


    public function getParams()
    {
        return $this->params;
    }


    public function getQueryParam($key)
    {
        $queryParams = $this->getQueryParams();

        return isset($queryParams[$key]) ? $queryParams[$key] : null;
    }


    public function getQueryParams()
    {
        $queryString = $this->getParam('QUERY_STRING');
        parse_str($queryString, $queryArr);

        return $queryArr;
    }


    public function getUrl($isSlash = false)
    {
        $uri = $isSlash ? $_SERVER['REQUEST_URI'] : ltrim($_SERVER['REQUEST_URI'], '/');

        return $uri;
    }


    public function getReferer()
    {
        return $_SERVER['HTTP_REFERER'];
    }

    /* TODO: HMVC
    static public function factory()
    {
        $request = new Request();
        $request->setParams();
        return $request;
    }
    */

}


?>