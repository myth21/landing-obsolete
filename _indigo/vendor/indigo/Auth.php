<?php

namespace indigo;

class Auth
{

    private static $_instance;

    /**
     * @var $_session Session
     */
    private $_session;

    private function init()
    {
        $this->_session = Session::getInstance();
    }

    static public function getInstance()
    {
        if (is_null(self::$_instance)) {
            self::$_instance = new self();
            self::$_instance->init();
        }
        return self::$_instance;
    }

    public function login($login, $pass)
    {
        $config = Config::getInstance();
        if ($login === $config->getParam('admin.login') && $pass === $config->getParam('admin.password')) {
            $this->_session->set('login', $login);
            $this->_session->set('password', $pass);
        }
    }

    public function logout()
    {
        $this->_session->delete('login');
        $this->_session->delete('password');
        $this->_session->destroySession();

        return true;
    }

    public function isLogged()
    {
        if ($this->_session->has('login') && $this->_session->has('password')) {
            if ($this->_session->get('login') && $this->_session->get('password')) {
                return true;
            }
        }
        return false;
    }

}