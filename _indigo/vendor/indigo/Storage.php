<?php

namespace indigo;

class Storage
{

    private static $_sqliteInstance = null;

    static public function getSqliteInstance()
    {
        if (is_null(self::$_sqliteInstance)) {
            self::$_sqliteInstance = new \PDO(Config::getInstance()->getParam('sqlite.dsn'));
        }
        self::$_sqliteInstance->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_WARNING);

        return self::$_sqliteInstance;
    }

}