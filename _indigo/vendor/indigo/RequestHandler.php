<?php

namespace indigo;

class RequestHandler
{

    static public function run()
    {
        $handler = new self();
        return $handler->handleRequest();
    }

    private function handleRequest()
    {
        $route = Router::getInstance();

        if (Server::getInstance()->isHttpRequest()) {

            /** @var $controller \indigo\Controller */
            $route->defineRoute();
            $controller = $route->getController();
            $response = $controller->execute();
            $response->sendHeaders();

            return $response->getBody();
        }

        /** @var $script \indigo\Console */
        $route->defineScript();
        $script = $route->getController();
        return $script->execute();

    }
}
