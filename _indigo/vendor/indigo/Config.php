<?php

namespace indigo;

class Config
{
    private static $_instance;

    private $_params = [];

    static public function getInstance()
    {
        if (self::$_instance === null) {
            self::$_instance = new self();
        }

        return self::$_instance;
    }

    public function setConfigFile($fileName, $ext = 'php', $overwrite = true)
    {
        $pathToFile = $fileName . '.' . $ext;

        if ($ext == 'php') {

            if (is_readable($pathToFile)) {
                $params = require_once $pathToFile;
            } else {
                throw new \ErrorException('Config file not found: ' . $pathToFile);
            }

            foreach ($params as $key => $value) {
                if ($overwrite == false && property_exists($this, $key)) {
                    continue;
                } else {
                    $this->setParam($key, $value);
                }
            }

        } else {
            throw new \ErrorException('Working to extension ' . $ext . ' is not implemented');
        }

        return $params;
    }

    private function setParam($property, $value)
    {
        $this->_params[$property] = $value;
    }

    public function getParam($key)
    {
        $params = $this->_params;
        $keyParams = explode('.', $key);
        $countParams = sizeof($keyParams);

        for ($i = 0; $i < $countParams; $i++) {
            if (array_key_exists($keyParams[$i], $params)) {
                $params = $params[$keyParams[$i]];
            } else {
                throw new \ErrorException('Config parameter ' . $keyParams[$i] . ' not found');
            }
        }

        return $params;
    }

    public function getParams()
    {
        return $this->_params;
    }

}