<?php
namespace indigo;

class Response
{
    private static $_instance;
    private $_body = '';
    private $_statusCode;

    public $statusMessage;

    public static $httpStatuses = [
        403 => 'Forbidden',
        404 => 'Not Found',
        500 => 'Internal Server Error',
        // ...
    ];

    static public function getInstance()
    {
        if (self::$_instance === null) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    public function setBody($data)
    {
        $this->_body = $data;
    }

    public function getBody()
    {
        return $this->_body;
    }

    static public function redirect($to = '', $code = 301)
    {
        header('Location:' . $to, true, $code);
        exit();
    }

    public function setStatusCode($code, $message = null)
    {
        if ($code === null) {
            $code = 200;
        }
        $this->_statusCode = (int)$code;
        if ($message === null) {
            $this->statusMessage = isset(static::$httpStatuses[$this->_statusCode]) ? static::$httpStatuses[$this->_statusCode] : '';
        } else {
            $this->statusMessage = $message;
        }
    }

    public function sendHeaders()
    {
        if (headers_sent()) {
            return;
        }

        $statusCode = $this->getStatusCode();
        $protocolVersion = Server::getInstance()->getProtocolVersion();
        header('HTTP/' . $protocolVersion . ' ' . $statusCode . ' ' . $this->statusMessage);
    }

    public function getStatusCode()
    {
        return $this->_statusCode;
    }

    public function getStatusMessage()
    {
        return $this->statusMessage;
    }

}