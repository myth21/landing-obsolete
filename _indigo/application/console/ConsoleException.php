<?php

class ConsoleException extends Console
{
    public function actionException()
    {
        $output = PHP_EOL . 'Controller not found or something is wrong' . PHP_EOL;
        \indigo\Console::exitScript($output);
    }
    
}