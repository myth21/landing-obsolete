<?php
$view = indigo\View::getInstance();
$config = indigo\Config::getInstance();
$server = indigo\Server::getInstance();
$route = indigo\Router::getInstance();
?>

<?php if (isset($page) && $page): ?>
    <h1><?php echo $page['title']; ?></h1>
    <?php if (isset($page['description'])): ?>
        <p><?php echo $page['description']; ?></p>
    <?php endif; ?>
<?php endif; ?>

<?php if (isset($news) && $news): ?>
    <div>
        <?php foreach ($news as $new): ?>
            <?php $url = $server->getProtocolHost() . $route->getUrl($controller = 'news', $action = 'view', ['id' => $new['id']]); ?>
            <h2>
                <a href="<?php echo $url ?>" title="<?php echo $new['title']; ?>"><?php echo $new['title']; ?></a>
            </h2>
            <div><img src="<?php echo $config->getParam('app.img') ?><?php echo $new['image_path']; ?>"></div>
            <p><?php echo $new['description']; ?></p>
        <?php endforeach; ?>
    </div>
<?php endif; ?>

<?php if ($view->isData('block')): ?>
    <blockquote>
        <h3>aBlock:</h3>
        <?php foreach ($view->getData('block') as $contentPartValue) echo $contentPartValue; ?>
    </blockquote>
<?php endif; ?>