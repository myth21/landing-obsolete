<?php
/** @var $this View */

$config = \indigo\Config::getInstance();
$server = \indigo\Server::getInstance();
$session = \indigo\Session::getInstance();
?>
<!DOCTYPE html>
<html>
<head>
    <title><?php echo $this->getTitle(); ?></title>
    <link rel="icon" type="image/png" href="/favicon.png"/>
    <link rel="stylesheet" href="<?php echo $config->getParam('app.css'); ?>bootstrap.min.css">
    <script type="text/javascript" src="<?php echo $config->getParam('app.js'); ?>functions.js"></script>
</head>
<body>
<div class="container">

    <?php echo $this->render('lang_form'); ?>

    <?php if ($this->isData('menu')): ?>
        <div class="row">
            <ul>
                <?php foreach ($this->getData('menu') as $menuKey => $menuValue): ?>
                    <li>
                        <a href="<?php echo $server->getProtocolHost() . $menuKey; ?>"
                           class="<?php echo $this->getLinkClassName($menuKey); ?>">
                            <?php echo $menuValue; ?>
                        </a>
                    </li>
                <?php endforeach; ?>
            </ul>
        </div>
    <?php endif; ?>

    <?php if ($session->has('message')): ?>
        <div class="row">
            <div class="alert alert-success">
                <?php echo $session->get('message', $delete = true); ?>
            </div>
        </div>
    <?php endif; ?>

    <?php if ($this->isContent()): ?>
        <div class="row">
            <?php foreach ($this->getContent() as $contentValue) echo $contentValue; ?>
        </div>
    <?php endif; ?>

    <?php if ($this->isData()): ?>
        <div class="row">
            <h3>Note:</h3>
            <?php foreach ($this->getData() as $contentPartValue) echo $contentPartValue; ?>
        </div>
    <?php endif; ?>

</div>
</body>
</html>