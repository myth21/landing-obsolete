<?php
$config = indigo\Config::getInstance();
?>

<h1><?php echo $new['title']; ?></h1>
<div>
    <img src="<?php echo $config->getParam('app.img') ?><?php echo $new['image_path']; ?>">
</div>
<p>
    <?php echo $new['description']; ?>
</p>