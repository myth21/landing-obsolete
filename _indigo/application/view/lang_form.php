<?php
use indigo\Cookie;
use indigo\Router;
use indigo\Language;

$config = indigo\Config::getInstance();
?>
<div class="row">
    <?php $lang = Cookie::get('language'); ?>
    <form action="<?php echo Router::getInstance()->getUrl($controller = 'language'); ?>" method="post" class="pull-right">
        <?php if ($lang && $lang == Language::EN_GB) : ?>
            <input type="hidden" name="language" value="<?php echo $config->getParam('app.defaultLanguage'); ?>">
            <input type="image" src="<?php echo $config->getParam('app.design'); ?>defaultFlag.png">
        <?php else: ?>
            <input type="hidden" name="language" value="<?php echo Language::EN_GB; ?>">
            <input type="image" src="<?php echo $config->getParam('app.design'); ?>enFlag.png">
        <?php endif; ?>
    </form>
</div>