<h1><?php echo $page['title']; ?></h1>
<p><?php echo $page['description']; ?></p>

<?php if (isset($settings) && $settings): ?>
    <div>
        <form method="post">
            <?php foreach ($settings as $setting): ?>
                <label for="<?php echo $setting['key']; ?>"><?php echo $setting['key']; ?></label>
                <input id="<?php echo $setting['key']; ?>" type="text" name="<?php echo $setting['key']; ?>"
                       value="<?php echo $setting['value']; ?>"><br>
            <?php endforeach; ?>
            <input type="submit" name="submit">
        </form>
    </div>
<?php endif; ?>