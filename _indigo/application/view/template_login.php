<?php
/** @var $this View */

use indigo\Language;
use indigo\Router;
use indigo\Session;

$config = indigo\Config::getInstance();
?>
<!DOCTYPE html>
<html>
<head>
    <title><?php echo $config->getParam('app.title'); ?></title>
    <link rel="icon" type="image/png" href="/favicon.png"/>
    <link rel="stylesheet" href="<?php echo $config->getParam('app.css'); ?>bootstrap.min.css">
</head>
<body>
<div class="container">

    <?php echo $this->render('lang_form'); ?>

    <div class="row">
        <h1><?php echo $config->getParam('app.title'); ?></h1>
        <hr/>
    </div>

    <div class="row">
        <form action="<?php echo Router::getInstance()->getUrl($controller = 'index', $action = 'login'); ?>"
              method="post" class="form-horizontal pull-left">
            <?php if (Session::getInstance()->has('loginFailed')): ?>
                <div class="alert alert-error">
                    <?php echo Session::getInstance()->get('loginFailed', $delete = true); ?>
                </div>
            <?php endif; ?>
            <div class="control-group">
                <label class="control-label" for="inputLogin"><?php echo Language::_('LOGIN'); ?></label>
                <div class="controls">
                    <input id="inputLogin" type="text" name="login">
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="inputPassword"><?php echo Language::_('PASSWORD'); ?></label>
                <div class="controls">
                    <input type="password" id="inputPassword" name="password">
                </div>
            </div>
            <div class="control-group">
                <div class="controls">
                    <input type="submit" name="submit" class="btn btn-primary"
                           value="<?php echo Language::_('LOGIN'); ?>">
                </div>
            </div>
        </form>
    </div>

</div>
</body>
</html>