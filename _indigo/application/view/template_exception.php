<?php
$config = \indigo\Config::getInstance();
?>
<!DOCTYPE html>
<html>
<head>
    <title><?php echo $config->getParam('app.title'); ?></title>
    <link rel="icon" type="image/png" href="/favicon.png"/>
    <link rel="stylesheet" href="<?php echo $config->getParam('app.css'); ?>bootstrap.min.css">
</head>
<body>
<div class="container">
            
    <div class="row">
        <div class="alert alert-error">
            <?php echo $data['statusCode'] . ' ' . $data['statusMessage']; ?>
        </div>
    </div>

</div>
</body>
</html>