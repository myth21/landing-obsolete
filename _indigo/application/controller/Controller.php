<?php

use indigo\Auth;
use indigo\Router;
use indigo\View;
use indigo\Language;
use indigo\Response;

abstract class Controller extends indigo\Controller
{
    public function before()
    {
        $route = Router::getInstance();
        $auth = Auth::getInstance();
        $authUrl = $route->getUrl($controller = 'index', $action = 'login');

        if($auth->isLogged())
        {
            $view = View::getInstance();
            
            $queryController = $route->getUrl($controller = $this->getName());
            $view->setQueryController($queryController);

            $view->addData([
                $route->getUrl($controller = 'index') => Language::_('HOME'),
                $route->getUrl($controller = 'settings') => Language::_('SETTINGS'),
                $route->getUrl($controller = 'index', $action = 'logout') => Language::_('LOGOUT'),
            ], 'menu');
        }
        elseif($this->request->getUrl() != $authUrl)
        {
            //Response::redirect($authUrl);
        }
    }

}