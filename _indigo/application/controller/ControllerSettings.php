<?php

use indigo\View;
use indigo\Response;
use indigo\Language;
use indigo\Request;

class ControllerSettings extends Controller
{
    
    public function ActionIndex()
    {
        $page = Model::factory('page');
        $settings = Model::factory('settings');
        $settingsData = $settings->getSettings();

        if (isset($_POST['submit']) && $_POST['submit']) {

            // TODO: method like setData($_POST);
            foreach ($settingsData as $key => $row)
            {
                if (isset($_POST[$row['key']])) {
                    $settingsData[$key]['value'] = $_POST[$row['key']];
                }
            }

            $result = $settings->update($settingsData);
            if ($result) {
                Response::redirect(Request::getInstance()->getUrl(true));
            }

            throw new \ErrorException('Removing failed');
        }

        $view = View::getInstance();
        $view->setTitle(Language::_('SETTINGS'));
        $view->setContent('settings', [
            'settings' => $settingsData,
            'page' => $page->getPage('settings'),
        ]);

        return $view->render();
    }


}