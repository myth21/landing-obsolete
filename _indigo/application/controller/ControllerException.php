<?php

use indigo\View;

class ControllerException extends Controller
{
    protected $template = 'template_exception';
    
    /** @var $view \indigo\View */
    protected $view;

    public function before()
    {
        $this->response = \indigo\Response::getInstance();
        $this->view = View::getInstance();
        $this->view->setToTemplate([
            'data' => [
                'statusCode' => $this->response->getStatusCode(),
                'statusMessage' => $this->response->getStatusMessage(),
            ],
        ]);
    }

    public function actionIndex()
    {
        throw new \ErrorException('Method ' . __METHOD__ . ' is not implemented');
    }

    public function action403()
    {
        return $this->view->render($this->template);
    }
    
    public function action404()
    {
        return $this->view->render($this->template);
    }
    
    public function action500()
    {
        return $this->view->render($this->template);
    }

}