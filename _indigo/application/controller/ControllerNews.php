<?php

use indigo\View;

class ControllerNews extends Controller
{
    public function ActionIndex()
    {
        throw new \ErrorException('Method ' . __METHOD__ . ' is not implemented');
    }

    public function ActionView()
    {
        $news = Model::factory('news');

        $id = $this->request->getParam('id');

        $new = $news->getNew($id);
        
        $view = View::getInstance();
        $view->setContent('new', [
            'new' => $new,
        ]);

        return $view->render();
    }

}