<?php

use indigo\Cookie;
use indigo\Language;
use indigo\Response;

class ControllerLanguage extends Controller
{
    public function before()
    {
        // Not auth checking
    }

    public function actionIndex()
    {
        if(isset($_POST['language']) && $_POST['language'] == Language::EN_GB)
        {
            Cookie::set('language', Language::EN_GB);
        }
        elseif(isset($_POST['language']) && $_POST['language'] == Language::RU_RU)
        {
            Cookie::delete('language');
        }

        $redirectUrl = $this->request->getReferer();
        Response::redirect($redirectUrl);
    }
    
}