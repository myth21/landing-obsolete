<?php

use indigo\View;
use indigo\Router;
use indigo\Auth;
use indigo\Response;
use indigo\Session;
use indigo\Language;

class ControllerIndex extends Controller
{

    public function actionIndex()
    {
        $page = Model::factory('page');
        $news = Model::factory('news');

        $view = View::getInstance();

        $view->setTitle(Language::_('HOME'));
        
        $view->addData([
            'Suspendisse ullamcorper ante non ultrices dapibus. Aenean ut ultricies ipsum.',
        ], 'block');
        
        $view->addData([
            'Nulla a odio tincidunt, congue lorem in, porttitor purus. Curabitur in pretium justo. Proin nec felis vel risus faucibus venenatis id sed magna. Morbi feugiat sem libero, et mattis magna consequat quis.'
        ]);

        $view->setContent('index', [
            'page' => $page->getPage('index'),
            'news' => $news->getNews()
        ]);

        return $view->render();
    }

    function actionLogin()
    {
        $auth = Auth::getInstance();

        if (isset($_POST['submit']))
        {
            $auth->login($_POST['login'], $_POST['password']);
            if ($auth->isLogged())
            {
                $redirectUrl = Router::getInstance()->getUrl($controller = 'index');
                Response::redirect($redirectUrl);
            } 
            else 
            {
                $this->response->setStatusCode(403);

                // log...

                Session::getInstance()->set('loginFailed', Language::_('LOGIN_PASSWORD_INCORRECT'));
            }
        }

        return View::getInstance()->render('template_login');
    }

    function actionLogout()
    {
        Auth::getInstance()->logout();
        $redirectUrl = Router::getInstance()->getUrl('index','login');
        Response::redirect($redirectUrl);
    }
}