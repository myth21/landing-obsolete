<?php

class ModelNews extends Model
{
    public function getNews()
    {
        $statement = $this->storage->query("SELECT * FROM news");
        return $statement->fetchAll(PDO::FETCH_ASSOC);
    }

    public function getNew($id)
    {
        $statement = $this->storage->query("SELECT * FROM news WHERE id = $id");
        $new = $statement->fetch(PDO::FETCH_ASSOC);
        return $new;
    }
    
}