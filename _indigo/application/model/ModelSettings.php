<?php

class ModelSettings extends Model
{

    public function getSettings()
    {
        $statement = $this->storage->query("SELECT * FROM settings");
        return $statement->fetchAll(PDO::FETCH_ASSOC);
    }

    public function update($settingsData)
    {
        $this->delete();
        $result = $this->insert($settingsData);
        return $result;
    }

    public function insert($settingsData)
    {
        $insertValues = $this->getInsertValues($settingsData);
        $statement = $this->storage->query("INSERT INTO settings (key, value) VALUES $insertValues");

        return $statement;
    }

    public function delete()
    {
        $statement = $this->storage->query("DELETE FROM settings");
        return $statement;
    }
    
    public function getInsertValues($settingsData)
    {
        $insertString = '';

        foreach ($settingsData as $settings)
        {
            $insertString .= '("' . $settings['key'] . '","' . $settings['value'] . '"),';
        }

        return substr($insertString, 0, -1);
    }

}