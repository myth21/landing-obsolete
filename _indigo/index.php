<?php

is_readable(__DIR__ . '/vendor/autoload.php') ? require_once __DIR__ . '/vendor/autoload.php' : exit('Auto loader not found');

/*
 * Set a config and run the application
 */

$server = indigo\Server::getInstance();
$config = indigo\Config::getInstance();

$config->setConfigFile($server->getDocRoot() . 'config' . DIRECTORY_SEPARATOR . 'common');

if ($config->getParam('developmentEnv')) {
    $config->setConfigFile($server->getDocRoot() . 'config' . DIRECTORY_SEPARATOR . 'development');
} else {
    $config->setConfigFile($server->getDocRoot() . 'config' . DIRECTORY_SEPARATOR . 'production');
}

echo indigo\RequestHandler::run();
