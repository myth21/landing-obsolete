<?php
// TODO move to app?
function getPage(\SQLite3 $db): array
{
    $data = $db->query('SELECT * FROM page')->fetchArray(SQLITE3_ASSOC);

    return $data ?: [];
}
function getConf(\SQLite3 $db): array
{
    $data = [];

    $query = $db->query('SELECT * FROM conf');
    while($entry = $query->fetchArray(SQLITE3_ASSOC)){
        $data[$entry['key']] = $entry['value'];
    }

    return $data;
}
function getAdvantage(\SQLite3 $db): array
{
    $model = $db->query('SELECT * FROM advantage')->fetchArray(SQLITE3_ASSOC);
    $query = $db->query('SELECT * FROM advantage_item WHERE parent_id='.$model['id']);

    $model['items'] = [];
    while($entry = $query->fetchArray(SQLITE3_ASSOC)){
        $model['items'][] = $entry;
    }
    $model['items_count'] = sizeof($model['items']);

    return $model;
}
function getOffer(\SQLite3 $db): array
{
    $data = $db->query('SELECT * FROM offer')->fetchArray(SQLITE3_ASSOC);

    return $data ?: [];
}
function getGallery(\SQLite3 $db): array
{
    $model = $db->query('SELECT * FROM gallery')->fetchArray(SQLITE3_ASSOC);
    $query = $db->query('SELECT * FROM gallery_item WHERE active=1 AND parent_id='.$model['id']);

    $model['items'] = [];
    while($entry = $query->fetchArray(SQLITE3_ASSOC)){
        $model['items'][] = $entry;
    }
    $model['items_count'] = sizeof($model['items']);

    return $model;
}
function getPlan(\SQLite3 $db): array
{
    $data = [];

    $query = $db->query('SELECT * FROM plan');
    while($entry = $query->fetchArray(SQLITE3_ASSOC)){
        $data[] = $entry;
    }

    return $data;
}
function getService(\SQLite3 $db): array
{
    $model = $db->query('SELECT * FROM service')->fetchArray(SQLITE3_ASSOC);
    $query = $db->query('SELECT * FROM service_item WHERE parent_id='.$model['id']);

    $model['items'] = [];
    while($entry = $query->fetchArray(SQLITE3_ASSOC)){
        $model['items'][] = $entry;
    }
    $model['items_count'] = sizeof($model['items']);

    return $model;
}
function getUse(\SQLite3 $db): array
{
    $model = $db->query('SELECT * FROM use')->fetchArray(SQLITE3_ASSOC);
    $query = $db->query('SELECT * FROM use_item WHERE parent_id='.$model['id']);

    $model['items'] = [];
    while($entry = $query->fetchArray(SQLITE3_ASSOC)){
        $model['items'][] = $entry;
    }
    $model['items_count'] = sizeof($model['items']);

    return $model;
}
function getMap(\SQLite3 $db): array
{
    $data = $db->query('SELECT * FROM map')->fetchArray(SQLITE3_ASSOC);

    return $data ?: [];
}
function getContact(\SQLite3 $db): array
{
    $model = $db->query('SELECT * FROM contact')->fetchArray(SQLITE3_ASSOC);
    $query = $db->query('SELECT * FROM contact_item WHERE parent_id='.$model['id']);

    $model['items'] = [];
    while($entry = $query->fetchArray(SQLITE3_ASSOC)){
        $model['items'][] = $entry;
    }
    $model['items_count'] = sizeof($model['items']);

    return $model;
}
function getContactForm(\SQLite3 $db): array
{
    $model = $db->query('SELECT * FROM form WHERE name="contact"')->fetchArray(SQLITE3_ASSOC);

    $query = $db->query('SELECT * FROM form_item WHERE parent_id='.$model['id']);

    $model['items'] = [];
    while($entry = $query->fetchArray(SQLITE3_ASSOC)){
        $model['items'][] = $entry;
    }
    $model['items_count'] = sizeof($model['items']);

    return $model;
}
function getOrderForm(\SQLite3 $db): array
{
    $data = $db->query('SELECT * FROM form WHERE name="order"')->fetchArray(SQLITE3_ASSOC);
    return $data ?: [];
}
function getVideo(\SQLite3 $db): array
{
    $data = $db->query('SELECT * FROM video')->fetchArray(SQLITE3_ASSOC);
    return $data ?: [];
}
function getProperty(\SQLite3 $db): array
{
    $model = $db->query('SELECT * FROM property')->fetchArray(SQLITE3_ASSOC);
    $query = $db->query('SELECT * FROM property_item WHERE parent_id='.$model['id']);

    $model['items'] = [];
    while($entry = $query->fetchArray(SQLITE3_ASSOC)){
        $model['items'][] = $entry;
    }
    $model['items_count'] = sizeof($model['items']);

    return $model;
}
