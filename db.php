<?php
namespace db;

function getSQLiteConnection(string $dbFileName): \SQLite3
{
    if (!is_readable($dbFileName)) {
        exit('Please check file '.$dbFileName);
    }

    return new \SQLite3($dbFileName);
}

function get(\SQLite3 $db, $table): array
{
    $data = $db->query('SELECT * FROM '.$table)->fetchArray(SQLITE3_ASSOC);

    return $data ?: [];
}

function getWhere(\SQLite3 $db, $table, $where = ''): array
{
    $data = $db->query('SELECT * FROM '.$table.' WHERE '.$where)->fetchArray(SQLITE3_ASSOC);

    return $data ?: [];
}