<?php if ($plans) { ?>
    <?php foreach ($plans as $plan) { ?>
        <div class="marginBottom10px">
            <i class="<?php echo $plan['class']; ?> bg-complement-1 faIcon marginRight5"></i>
            <?php echo $plan['desc']; ?> <?php echo $plan['price']; ?>
        </div>
    <?php } ?>
    <div class="marginBottom30"></div>
<?php } ?>