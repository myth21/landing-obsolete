<div class="col col-12">

    <div class="marginBottom40px">
        <h1 class="h1 upperCase">
            <a href="/" title="<?php echo $model['title']; ?>" class="primary-2"><?php echo $model['title']; ?></a>
        </h1>
    </div>

    <div class="col col-6">

        <div class="lineHeight25 grey marginBottom40px">
            <?php echo $model['meta_desc']; ?>
        </div>

        <div class="marginBottom15px lineHeight25">
            <?php echo $model['about']; ?>
        </div>

        <?php foreach ($model['plans'] as $plan) { ?>
            <div class="marginBottom10px">
                <i class="<?php echo $plan['class']; ?> bg-complement-1 faIcon marginRight5"></i>
                <?php echo $plan['desc']; ?> <?php echo $plan['price']; ?>
            </div>
        <?php } ?>
        <div class="marginBottom30"></div>

    </div>

    <div class="col col-1">&nbsp;</div>

    <div class="col col-5">

        <div class="marginBottom60px">

            <?php /*if ($about) { ?>
                <div class="marginBottom10 lineHeight25"><?php echo $about; ?></div>
            <?php }*/ ?>
            <div class="marginBottom20px">
                Тел. <?php echo functions\getHumanRuPhoneNumber($model['contact_phone']); ?> <?php //echo $contact_name; ?>
            </div>
            <?php if ($model['is_whatsapp']) { ?>
                <div class="marginBottom20px">
                    <a class="btn white whatsAppBgColor" href="https://wa.me/<?php echo $model['contact_phone']; ?>">Написать в WhatsApp</a>
                </div>
            <?php } ?>
            <?php if ($model['contact_tg_nick']) { ?>
                <div class="marginBottom20px">
                    <a class="btn white bgTg" href="tg://resolve?domain=<?php echo $model['contact_tg_nick']; ?>">Написать в Telegram &nbsp;</a>
                </div>
            <?php } ?>
        </div>

        <form id="contactForm" action="/request-handler.php" class="hidden">
            <div class="marginBottom20px"><input type="email" class="input" id="email" name="email" placeholder="Email" required></div>
            <div class="marginBottom20px"><textarea class="input" name="message" id="message" placeholder="Сообщение" required></textarea></div>
            <div>
                <button type="submit" class="btn white bg-primary-2" id="messageSubmit">Отправить</button>
            </div>
        </form>
        <div id="contactFormSentSuccessfully" class="opacityZero primary-2 bold">Успешно отправленно</div>
        <script>
            document.addEventListener('DOMContentLoaded', () => {
                let contactForm = document.getElementById('contactForm');
                contactForm.classList.remove('hidden');
                contactForm.addEventListener('submit', (e) => {
                    e.preventDefault();
                    let params = {
                        "email": document.getElementById('email').value,
                        "message": document.getElementById('message').value,
                    };
                    sendPost(contactForm.action, getHttpBuildUrl(params), function(responseText){
                        if (responseText === 'OK') {
                            hideElement(contactForm, function(){
                                let el = document.getElementById('contactFormSentSuccessfully');
                                el.classList.remove('opacityZero');
                                el.classList.add('opacityOneTransition');
                            });
                        }
                    });
                });
            });
        </script>

    </div>

</div>