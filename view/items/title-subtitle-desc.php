<?php foreach ($model['items'] as $item) { ?>
    <div class="<?php echo $itemClass; ?>">
        <?php if ($item['title']) { ?>
            <h3 class="<?php echo $itemTitleClass; ?>"><?php echo $item['title']; ?></h3>
        <?php } ?>
        <?php if ($item['subtitle']) { ?>
            <div class="<?php echo $itemSubtitleClass; ?>"><?php echo $item['subtitle']; ?></div>
        <?php } ?>
        <?php if ($item['desc']) { ?>
            <div class="<?php echo $itemDescClass; ?>"><?php echo $item['desc']; ?></div>
        <?php } ?>
    </div>
<?php } ?>

