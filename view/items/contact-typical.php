<?php foreach ($model['items'] as $item) { ?>
    <div class="<?php echo $itemClass; ?>">

        <?php if ($item['icon']) { ?>
            <div class="marginBottom20px <?php //echo $itemIconClass; ?>"><?php echo $item['icon']; ?></div>
        <?php } ?>

        <?php if ($item['img']) { ?>
            <div class="marginBottom20px <?php //echo $itemImgClass; ?>"><?php echo $item['img']; ?></div>
        <?php } ?>

        <?php if ($item['name']) { ?>
            <div class="marginBottom20px <?php //echo $itemNameClass; ?>"><?php echo $item['name']; ?></div>
        <?php } ?>

        <?php if ($item['email']) { ?>
            <div class="marginBottom20px <?php //echo $itemEmailClass; ?>">
                <a class="<?php //echo $itemEmailLinkClass; ?>" href="mailto:<?php echo $item['email']; ?>"><?php echo $item['email']; ?></a>
            </div>
        <?php } ?>

        <?php if ($item['phone']) { ?>
            <div class="marginBottom20px <?php //echo $itemEmailClass; ?>">
                <a class="<?php //echo $itemEmailLinkClass; ?>" href="tel:<?php echo $item['phone']; ?>"><?php echo functions\getHumanRuPhoneNumber($item['phone']); ?></a>
            </div>
        <?php } ?>

        <?php if ($item['is_whatsapp']) { ?>
            <div class="marginBottom20px">
                <a class="btn whatsAppBgColor colorWhite" href="https://wa.me/<?php echo $item['phone']; ?>">WhatsApp</a>
            </div>
        <?php } ?>

        <?php if ($item['telegram_id']) { ?>
            <div class="marginBottom20px">
                <a class="btn telegramBackgroundColor colorWhite" href="tg://resolve?domain=<?php echo $item['telegram_id']; ?>"> Telegram </a>
            </div>
        <?php } ?>

        <?php if ($item['address']) { ?>
            <div class="marginBottom20px"><?php echo $item['address']; ?></div>
        <?php } ?>

    </div>
<?php } ?>

