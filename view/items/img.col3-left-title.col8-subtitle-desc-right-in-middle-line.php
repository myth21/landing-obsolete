<?php foreach ($model['items'] as $item) { ?>

    <div class="<?php echo $itemClass; ?>">

        <div class="col-3 inMiddleBlockLine">
            <img class="<?php echo $itemImgClass; ?>" src="/app/<?php echo $item['img']; ?>" alt="image">
        </div>

        <div class="col-1 inMiddleBlockLine">&nbsp;</div>

        <div class="col-8 inMiddleBlockLine">
            <?php if ($model['title']) { ?>
                <h3 class="<?php echo $itemTitleClass; ?>"><?php echo $model['title']; ?></h3>
            <?php } ?>

            <?php if ($model['subtitle']) { ?>
                <div class="<?php echo $itemSubtitleClass; ?>"><?php echo $model['subtitle']; ?></div>
            <?php } ?>

            <?php if ($model['desc']) { ?>
                <div class="<?php echo $itemDescClass; ?>"><?php echo $model['desc']; ?></div>
            <?php } ?>
        </div>

    </div>

<?php } ?>

