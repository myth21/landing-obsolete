<?php foreach ($model['items'] as $item) { ?>
    <div class="col col-<?php echo $colCount ?> <?php echo $itemClass; ?>">

        <?php if ($item['icon']) { ?>
            <div class="<?php echo $itemIconClass ?>">
                <span class="fa-stack fa-<?php echo $colCount; ?>x">
                  <i class="fa fa-circle fa-stack-2x"></i>
                  <i class="fa <?php echo $item['icon']; ?> fa-stack-1x primary2"></i>
                </span>
            </div>
        <?php } ?>

        <?php if ($item['title']) { ?>
            <div class="<?php echo $itemTitleClass; ?>"><?php echo $item['title']; ?></div>
        <?php } ?>

        <?php if ($item['subtitle']) { ?>
            <div class="<?php echo $itemSubtitleClass; ?>"><?php echo $item['subtitle']; ?></div>
        <?php } ?>

        <?php if ($item['desc']) { ?>
            <div class="<?php echo $itemDescClass; ?>"><?php echo $item['desc']; ?></div>
        <?php } ?>

    </div>
<?php } ?>