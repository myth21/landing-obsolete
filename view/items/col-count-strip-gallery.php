<?php foreach ($model['items'] as $item) { ?>
    <div class="col col-<?php echo $colCount ?>">
        <div class="<?php echo $itemClass ?>">
            <a href="/app/<?php echo $item['name']; ?>" class="strip" data-strip-group="<?php echo $item['group']; ?>" data-strip-caption="<?php echo $item['caption']; ?>">
                <img class="" src="/app/<?php echo $item['name']; ?>" alt="<?php echo $item['alt']; ?>">
            </a>
        </div>
    </div>
    <?php if ($i % $clearBothCount === 0) { ?>
        <div class="clearBoth"></div>
    <?php } ?>
    <?php $i++; ?>
<?php } ?>