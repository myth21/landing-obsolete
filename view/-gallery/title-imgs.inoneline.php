<div class="col col-12 <?php echo $colClass; ?>">

    <?php if ($model['title']) { ?>
        <h1 class="<?php echo $titleClass; ?>"><?php echo $model['title']; ?></h1>
    <?php } ?>

    <?php if ($model['subtitle']) { ?>
        <h1 class="<?php echo $subtitleClass; ?>"><?php echo $model['subtitle']; ?></h1>
    <?php } ?>

    <?php if ($model['desc']) { ?>
        <div class="<?php echo $descClass; ?>"><?php echo $model['desc']; ?></div>
    <?php } ?>

    <?php if ($model['items']) { ?>
        <?php $colCount = $colBaseNumber/$model['items_count']; ?>
        <?php foreach ($model['items'] as $item) { ?>
            <div class="col col-<?php echo $colCount ?>">
                <div class="stripImgWrapper">
                    <a href="/app/<?php echo $item['name']; ?>" class="strip" data-strip-group="<?php echo $item['group']; ?>" data-strip-caption="<?php echo $item['caption']; ?>">
                        <img class="" src="/app/<?php echo $item['name']; ?>" alt="<?php echo $item['alt']; ?>">
                    </a>
                </div>
            </div>
        <?php } ?>
        <div class="clearBoth">&nbsp;</div>
    <?php } ?>

    <?php if ($model['after']) { ?>
        <div class="<?php echo $afterClass; ?>"><?php echo $model['after']; ?></div>
    <?php } ?>

</div>

