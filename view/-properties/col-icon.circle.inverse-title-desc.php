<div class="col col-12 <?php echo $colClass; ?>">

    <?php if($title) { ?>
        <h2 class="<?php echo $titleClass; ?>"><?php echo $title; ?></h2>
    <?php } ?>

    <?php $colCount = $colBaseNumber/count($models); ?>
    <?php foreach ($models as $model) { ?>

        <div class="col col-<?php echo $colCount ?> marginBottom20px">

            <div class="textAlignCenter marginBottom10px">
                <span class="fa-stack fa-<?php echo $colCount; ?>x">
                  <i class="fa fa-circle fa-stack-2x"></i>
                  <i class="fa <?php echo $model['icon_class']; ?> fa-stack-1x fa-inverse primary-2"></i>
                </span>
            </div>

            <?php if($model['title']) { ?>
                <h3 class="<?php echo $model['title_class']; ?>"><?php echo $model['title']; ?></h3>
            <?php } ?>

            <?php if($model['desc']) { ?>
                <div class="<?php echo $model['desc_class']; ?>"><?php echo $model['desc']; ?></div>
            <?php } ?>
        </div>

    <?php } ?>
</div>