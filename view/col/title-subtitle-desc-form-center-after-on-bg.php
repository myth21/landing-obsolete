<div class="col col-12 <?php echo $colClass; ?>" style="<?php echo $colStyle; ?>">

    <?php if ($model['title']) { ?>
        <h2 class="<?php echo $titleClass; ?>"><?php echo $model['title']; ?></h2>
    <?php } ?>

    <?php if ($model['subtitle']) { ?>
        <h3 class="<?php echo $subtitleClass; ?>"><?php echo $model['subtitle']; ?></h3>
    <?php } ?>

    <?php if ($model['desc']) { ?>
        <div class="<?php echo $descClass; ?>"><?php echo $model['desc']; ?></div>
    <?php } ?>

    <div class="col col-3">&nbsp;</div>
    <div class="col col-6">

        <form method="post" id="<?php echo $model['name']; ?>Form" action="/request-handler.php">
            <input type="hidden" name="<?php echo $model['name']; ?>Token" value="<?php echo $_SESSION['token'];//\functions\getSession('token'); ?>">
            <?php if ($model['user_name']) { ?>
                <div class="marginBottom20px">
                    <input type="text" class="input" id="user_name" name="user_name" placeholder="Name" required>
                </div>
            <?php } ?>

            <?php if ($model['user_email']) { ?>
                <div class="marginBottom20px">
                    <input type="email" class="input" id="user_email" name="user_email" placeholder="Email" required>
                </div>
            <?php } ?>

            <?php if ($model['user_phone']) { ?>
                <div class="marginBottom20px">
                    <input type="text" class="input" id="user_phone" name="user_phone" placeholder="Phone" required>
                </div>
            <?php } ?>

            <?php if ($model['user_message']) { ?>
                <div class="marginBottom20px">
                    <textarea class="input" name="user_message" id="user_message" placeholder="Message" required></textarea>
                </div>
            <?php } ?>

            <?php if ($model['user_contacts']) { ?>
                <div class="marginBottom20px">
                    <input type="text" class="input" id="user_contacts" name="user_contacts" placeholder="Contacts">
                </div>
            <?php } ?>

            <div class="<?php echo $model['name']; ?>FormSubmit">
                <button type="submit" class="btn bgSecondary1 colorWhite" id="<?php echo $model['name']; ?>FormSubmit"><?php echo $model['button']; ?></button>
            </div>
        </form>
        <?php echo \functions\getFile('view'.DIRECTORY_SEPARATOR.'part'.DIRECTORY_SEPARATOR.'modal.php'); ?>
        <script>
            document.addEventListener('DOMContentLoaded', () => {
                setFormHandler('<?php echo $model["name"]; ?>Form');
            });
        </script>

        <?php if (isset($model['after']) && $model['after']) { ?>
            <div class="<?php echo $afterClass; ?>"><?php echo $model['after']; ?></div>
        <?php } ?>

    </div>
    <div class="col col-3">&nbsp;</div>

</div>