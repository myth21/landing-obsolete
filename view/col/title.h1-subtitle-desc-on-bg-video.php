<div class="col col-12 <?php echo $colClass; ?>" style="<?php echo $colStyle; ?>">

    <div class="bgVideo">

        <video loop muted autoplay poster="/app/poster.jpg">
            <source src="/app/cover.mp4" type="video/mp4">
        </video>

        <div class="bgVideoOverlay">

            <?php if ($model['title']) { ?>
                <h1 class="<?php echo $titleClass; ?>"><?php echo $model['title']; ?></h1>
            <?php } ?>

            <?php if ($model['subtitle']) { ?>
                <div class="<?php echo $subtitleClass; ?>"><?php echo $model['subtitle']; ?></div>
            <?php } ?>

            <?php if ($model['desc']) { ?>
                <div class="<?php echo $descClass; ?>"><?php echo $model['desc']; ?></div>
            <?php } ?>

        </div>

    </div>

</div>