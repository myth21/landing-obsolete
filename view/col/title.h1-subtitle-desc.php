<?php
/** @var $colClass string */
/** @var $colStyle string */
/** @var $model array */
/** @var $titleClass string */
/** @var $subtitleClass string */
/** @var $descClass string */
?>
<div class="col col-12 <?php echo $colClass; ?>" style="<?php echo $colStyle; ?>">

    <?php if ($model['title']) { ?>
        <h1 class="<?php echo $titleClass; ?>"><?php echo $model['title']; ?></h1>
    <?php } ?>

    <?php if ($model['subtitle']) { ?>
        <div class="<?php echo $subtitleClass; ?>"><?php echo $model['subtitle']; ?></div>
    <?php } ?>

    <?php if ($model['desc']) { ?>
        <div class="<?php echo $descClass; ?>"><?php echo $model['desc']; ?></div>
    <?php } ?>

</div>