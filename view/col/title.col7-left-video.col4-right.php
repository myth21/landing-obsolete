<div class="col col-12 <?php echo $colClass; ?>" style="<?php echo $colStyle; ?>">

    <div class="col col-4">

        <?php if ($model['title']) { ?>
            <h2 class="<?php echo $titleClass; ?>"><?php echo $model['title']; ?></h2>
        <?php } ?>

        <?php if ($model['subtitle']) { ?>
            <div class="<?php echo $subtitleClass; ?>"><?php echo $model['subtitle']; ?></div>
        <?php } ?>

        <?php if ($model['desc']) { ?>
            <div class="<?php echo $descClass; ?>"><?php echo $model['desc']; ?></div>
        <?php } ?>

    </div>

    <div class="col col-1">&nbsp;</div>

    <div class="col col-7">
        <video controls poster="/app/poster.jpg" class="video">
            <source src="/app/<?php echo $model['video']; ?>" type="video/mp4">
        </video>
    </div>

</div>