<div class="col col-12 <?php echo $colClass; ?>" style="<?php echo $colStyle; ?>">

    <?php if ($model['img']) { ?>
        <div class="col col-5">
            <img class="imgResponsive <?php echo $imgClass; ?>" src="/app/<?php echo $model['img']; ?>" alt="image">
        </div>
    <?php } ?>

    <div class="col col-1">&nbsp;</div>

    <div class="col col-6">

        <?php if ($model['title']) { ?>
            <h2 class="<?php echo $titleClass; ?>"><?php echo $model['title']; ?></h2>
        <?php } ?>

        <?php if ($model['subtitle']) { ?>
            <div class="<?php echo $subtitleClass; ?>"><?php echo $model['subtitle']; ?></div>
        <?php } ?>

        <?php if ($model['desc']) { ?>
            <div class="<?php echo $descClass; ?>"><?php echo $model['desc']; ?></div>
        <?php } ?>

    </div>

</div>