<div class="col col-12 <?php echo $colClass; ?>" style="<?php echo $colStyle; ?>">

    <?php if ($model['img']) { ?>
        <div class="col col-4">
            <img class="imgResponsive <?php echo $imgClass; ?>" src="/app/<?php echo $model['img']; ?>" alt="image">
        </div>
    <?php } ?>

    <div class="col col-1">&nbsp;</div>

    <?php if (isset($model['items']) && $model['items'] && $itemsView) { ?>
        <div class="col col-7 <?php echo $itemsClass; ?>">
            <?php echo \functions\getFileContent($itemsView, [
                    'model' => $model,
                    'colBaseNumber' => $colBaseNumber,
                    'colCount' => $colBaseNumber / $model['items_count'] * $itemsRowCount,
                    'itemsRowCount' => $itemsRowCount,
                    'itemClass' => $itemClass,
                    'itemTitleClass' => $itemTitleClass,
                    'itemSubtitleClass' => $itemSubtitleClass,
                    'itemDescClass' => $itemDescClass,
                    'itemImgClass' => $itemImgClass,
                    'itemIconClass' => $itemIconClass,
                    'clearBothCount' => $model['items_count'] / $itemsRowCount,
                    'i' => 1,
            ]); ?>
        </div>
    <?php } ?>

    <div class="clearBoth"></div>

</div>