<div class="col col-12 <?php echo $colClass; ?>" style="<?php echo $colStyle; ?>">

    <?php if ($model['title']) { ?>
        <h2 class="<?php echo $titleClass; ?>"><?php echo $model['title']; ?></h2>
    <?php } ?>

    <?php if ($model['subtitle']) { ?>
        <div class="<?php echo $subtitleClass; ?>"><?php echo $model['subtitle']; ?></div>
    <?php } ?>

    <?php if ($model['desc']) { ?>
        <div class="<?php echo $descClass; ?>"><?php echo $model['desc']; ?></div>
    <?php } ?>

    <?php if (isset($model['items']) && $model['items'] && $itemsView) { ?>
        <div class="<?php echo $itemsClass; ?>">
            <?php echo \functions\getFileContent($itemsView, [
                    'model' => $model,
                    'colBaseNumber' => $colBaseNumber,
                    'colCount' => $colBaseNumber / $model['items_count'] * $itemsRowCount,
                    'itemsRowCount' => $itemsRowCount,
                    'clearBothCount' => $model['items_count'] / $itemsRowCount,
                    'itemClass' => $itemClass,
                    'itemTitleClass' => $itemTitleClass,
                    'itemSubtitleClass' => $itemSubtitleClass,
                    'itemDescClass' => $itemDescClass,
                    'itemIconClass' => $itemIconClass,
                    'itemImgClass' => $itemImgClass,
                    'i' => 1,
            ]); ?>
        </div>
    <?php } ?>

    <?php if (isset($model['after']) && $model['after']) { ?>
        <div class="<?php echo $afterClass; ?>"><?php echo $model['after']; ?></div>
    <?php } ?>

</div>