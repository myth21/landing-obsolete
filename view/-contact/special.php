<div class="col col-6" style="">

    <div class="col col-3" style="">
        <?php if ($contact_img) { ?>
            <img src="/web/img/<?php echo $contact_img; ?>" class="round" alt="">
        <?php } ?>
    </div>
    <div class="col col-1" style="">&nbsp;</div>
    <div class="col col-8" style="">

        <?php if ($about) { ?>
            <div class="marginBottom20px"><?php echo $about; ?></div>
        <?php } ?>

        <div class="marginBottom20px">
            <?php if ($is_whatsapp) { ?>
                <a class="primary-1" href="https://wa.me/<?php echo $contact_phone; ?>"><?php echo functions\getHumanRuPhoneNumber($contact_phone); ?> (WhatsApp)</a>
            <?php } else { ?>
                <?php echo $contact_phone; ?>
            <?php } ?>
        </div>

        <?php if ($contact_tg_nick) { ?>
            <div class="marginBottom20px"><a class="primary-1" href="tg://resolve?domain=<?php echo $contact_tg_nick; ?>">@<?php echo $contact_tg_nick; ?> (Telegram)</a></div>
        <?php } ?>

        <div><?php echo $contact_name; ?></div>
    </div>
</div>
<div class="col col-1">&nbsp;</div>
<div class="col col-5">
    <span id="contactFormSentSuccessfully" class="primary-1 opacityZero">Успешно отправленно</span>
    <form id="contactForm" action="/request-handler.php" class="hidden">
        <div class="marginBottom20px"><input type="email" class="input" id="email" name="email" placeholder="Email" required></div>
        <div class="marginBottom20px"><textarea class="input" name="message" id="message" placeholder="Сообщение" required></textarea></div>
        <div>
            <button type="submit" class="btn white bg-primary-1" id="messageSubmit">Отправить</button>
        </div>
    </form>
    <script>
        document.addEventListener('DOMContentLoaded', () => {
            let contactForm = document.getElementById('contactForm');
            contactForm.classList.remove('hidden');
            contactForm.addEventListener('submit', (e) => {
                e.preventDefault();
                let params = {
                    'email': document.getElementById('email').value,
                    'message': document.getElementById('message').value,
                };
                sendPost(contactForm.action, getHttpBuildUrl(params), function(responseText){
                    if (responseText === 'OK') {
                        hideElement(contactForm, function(){
                            let el = document.getElementById('contactFormSentSuccessfully');
                            el.classList.remove('opacityZero');
                            el.classList.add('opacityOneTransition');
                        });
                    }
                });
            });
        });
    </script>
</div>