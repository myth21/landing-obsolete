<div class="col col-12">

    <?php if($title) { ?><h2><?php echo $title; ?></h2><?php } ?>

    <div class="cardGroups marginBottom40px">
        <?php foreach ($models as $model) { ?>
            <?php if(!$model['type_id']) { ?>
                <?php if(isset($model['title']) && $model['title']) { ?>
                    <div class="<?php //echo $model['title_class']; ?>cardGroup"><?php echo $model['title']; ?></div>
                <?php } ?>
            <?php } ?>
        <?php } ?>
    </div>

    <div class="cardGroups">
        <?php foreach ($models as $model) { ?>
            <?php if($model['type_id']) { ?>
                <?php if(isset($model['title']) && $model['title']) { ?>
                    <div class="<?php //echo $model['title_class']; ?>cardGroup"><?php echo $model['title']; ?></div>
                <?php } ?>
            <?php } ?>
        <?php } ?>
    </div>

</div>