<?php
session_start();
require_once 'error-handler.php';
require_once 'functions.php';
require_once 'db.php';
require_once 'model.php';
$config = require_once 'app'.DIRECTORY_SEPARATOR.'config.php';

$_SESSION['token'] = \functions\getRandomToken();
//\functions\setSession('token', \functions\getRandomToken());
//sleep(1);
//var_dump($_SESSION);

$dbConnection = db\getSQLiteConnection($config['sqliteDbFileName']);
foreach ($config['rowMap'] as $modelName => $rowConfig)
{
    $functionName = 'get'.ucfirst($modelName);
    $rows[] = [
        // Layout data
        'rowId' =>  $rowConfig['rowId'] ?? '',
        'rowClass' => $rowConfig['rowClass'] ?? '',
        'rowStyle' => $rowConfig['rowStyle'] ?? '',
        'boxClass' => $rowConfig['boxClass'] ?? '',
        'boxStyle' => $rowConfig['boxStyle'] ?? '',
        // View data and items
        'content' => functions\getFileContent($rowConfig['viewPath'], [
            'model' => $functionName($dbConnection),

            'colClass' => $rowConfig['colClass'] ?? '',
            'colStyle' => $rowConfig['colStyle'] ?? '',

            'imgClass' => $rowConfig['imgClass'] ?? '',
            'titleClass' => $rowConfig['titleClass'] ?? '',
            'subtitleClass' => $rowConfig['subtitleClass'] ?? '',
            'descClass' => $rowConfig['descClass'] ?? '',
            'afterClass' => $rowConfig['afterClass'] ?? '',

            'colBaseNumber' => $config['colBaseNumber'] ?? 12,

            'itemsView' => $rowConfig['itemsView'] ?? '',
            'itemsRowCount' => $rowConfig['itemsRowCount'] ?? 1,
            'itemsClass' => $rowConfig['itemsClass'] ?? '',
            'itemClass' => $rowConfig['itemClass'] ?? '',
            'itemTitleClass' => $rowConfig['itemTitleClass'] ?? '',
            'itemSubtitleClass' => $rowConfig['itemSubtitleClass'] ?? '',
            'itemDescClass' => $rowConfig['itemDescClass'] ?? '',
            'itemIconClass' => $rowConfig['itemIconClass'] ?? '',
            'itemImgClass' => $rowConfig['itemImgClass'] ?? '',
        ]),
    ];
}
$html = functions\getFileContent('layout.php', [
    'page' => getPage($dbConnection),
    'style' => functions\getCssString($config['style']),
    'headJs' => functions\getJsString($config['headJs']),
    'bodyJs' => functions\getJsString($config['bodyJs']),
    'rows' => isset($rows) ? $rows : []
]);
//echo functions\minifyHtml($html);
echo $html;