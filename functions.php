<?php
namespace functions;

/**
 * @param string $fileName
 * @param array $data
 * @return string
 */
function getFileContent($fileName, array $data = [])
{
    ob_start();
    extract($data);
    require $fileName;

    return ob_get_clean();
}

function getFile($fileName)
{
    ob_start();
    require $fileName;

    return ob_get_clean();
}

function getCssString(array $fileNames = [])
{
    $out = [];
    foreach ($fileNames as $fileName => $isMinify) {
        $fileContent = file_get_contents($fileName);
        $out[] = $isMinify ? minifyCss($fileContent) : $fileContent;
    }

    return implode(' ', $out);
}

function getJsString(array $fileNames = [])
{
    $out = [];
    foreach ($fileNames as $fileName => $isMinify) {
        $fileContent = file_get_contents($fileName);
        $out[] = $isMinify ? minifyJs($fileContent) : $fileContent;
    }

    return implode(' ', $out);
}

function minifyCss($string)
{
    $string = preg_replace('/\/\*((?!\*\/).)*\*\//','',$string); // negative look ahead
    $string = preg_replace('/\s{2,}/',' ',$string);
    $string = preg_replace('/\s*([:;{}])\s*/','$1',$string);
    $string = preg_replace('/;}/','}',$string);

    return $string;
}

function minifyJs($string) {

    // special case where the + indicates treating variable as numeric, e.g. a = b + +c
    $string = preg_replace('/([-\+])\s+\+([^\s;]*)/','$1 (+$2)',$string);
    // condense spaces
    $string = preg_replace("/\s*\n\s*/","\n",$string); // spaces around newlines
    $string = preg_replace("/\h+/"," ",$string); // \h+ horizontal white space
    // remove unnecessary horizontal spaces around non variables (alphanumerics, underscore, dollarsign)
    $string = preg_replace("/\h([^A-Za-z0-9\_\$])/",'$1',$string);
    $string = preg_replace("/([^A-Za-z0-9\_\$])\h/",'$1',$string);
    // remove unnecessary spaces around brackets and parantheses
    $string = preg_replace("/\s?([\(\[{])\s?/",'$1',$string);
    $string = preg_replace("/\s([\)\]}])/",'$1',$string);
    // remove unnecessary spaces around operators that don't need any spaces (specifically newlines)
    $string = preg_replace("/\s?([\.=:\-+,])\s?/",'$1',$string);
    // unnecessary characters
    $string = preg_replace("/;\n/",";",$string); // semicolon before newline
    $string = preg_replace('/;}/','}',$string); // semicolon before end bracket

    return $string;
}
function minifyHtml($string) {

    $search = [
        '/\>[^\S ]+/s',     // strip whitespaces after tags, except space
        '/[^\S ]+\</s',     // strip whitespaces before tags, except space
        '/(\s)+/s',         // shorten multiple whitespace sequences
        '/<!--(.|\s)*?-->/', // Remove HTML comments
    ];
    $replace = [
        '>',
        '<',
        '\\1',
        '',
    ];

    $string = preg_replace($search, $replace, $string);

    return $string;
}

function getHumanRuPhoneNumber($cleanedNumber)
{
    $number = $cleanedNumber;

    if (strlen($cleanedNumber) === 11) {

        $phone_number['dialcode'] = substr($cleanedNumber, 0, 1);
        $phone_number['code']  = substr($cleanedNumber, 1, 3);
        $phone_number['phone'] = substr($cleanedNumber, -7); // remaining digitals
        $phone_number['phone_arr'][] = substr($phone_number['phone'], 0, 3); // xxx
        $phone_number['phone_arr'][] = substr($phone_number['phone'], 3, 2); // xx
        $phone_number['phone_arr'][] = substr($phone_number['phone'], 5, 2); // xx

        $formatted_number = '+' . $phone_number['dialcode'] . ' ('. $phone_number['code'] .') ' . implode('-', $phone_number['phone_arr']);

        $number = $formatted_number;
    }

    return $number;
}

function getHttpResponseCode($url)
{
    stream_context_set_default([
        'http' => [
            'method' => 'HEAD'
        ]
    ]);
    $headers = get_headers($url);

    return substr($headers[0], 9, 3);
}

function isHttpResponseCodeOk($url)
{
    return getHttpResponseCode($url) == '200';
}

function getRandomToken() {
    return bin2hex(random_bytes(16));
}

function setSession($key, $value) {
    $_SESSION[$key] = $value;
}
function getSession($key) {
    return $_SESSION[$key];
}
function delSession($key) {
    unset($_SESSION[$key]);
}