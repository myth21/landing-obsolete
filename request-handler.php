<?php // TODO User can send one form only!
session_start();
require_once 'error-handler.php';
require_once 'functions.php';
require_once 'db.php';
require_once 'model.php';
// AJAX handler?
$config = require_once 'app'.DIRECTORY_SEPARATOR.'config.php';
$dbConnection = db\getSQLiteConnection($config['sqliteDbFileName']);
$dbConfig = getConf($dbConnection);
/*
 * On client form data should be clear better, but not hide
 */
function sendEmail($dbConfig, $config, $body)
{
    return mail($dbConfig['adminEmail'], 'Message from ' . $config['host'], $body, 'From: <'.$dbConfig['fromEmail'].'>');
}
function isValidRequest($keyName)
{
    if ($_POST) {
        if (isset($_POST[$keyName]) && isset($_SESSION['token'])) {
            if (hash_equals($_POST[$keyName], $_SESSION['token'])) {
                return true;
            }
        }
    }

    return false;
}
function getPostBody()
{
    $body = '';
    foreach ($_POST as $key => $value) {
        $body .= $key . ': ' . $value . "\n";
    }
    return $body;
}

$sent = false;
if (isValidRequest('orderToken'))
{
    $sent = sendEmail($dbConfig, $config, getPostBody());
}
elseif (isValidRequest('contactToken'))
{
    $sent = sendEmail($dbConfig, $config, getPostBody());
}

if ($sent) {
    $response = [
        'status' => 'OK',
        'message' => $dbConfig['true'],
    ];
} else {
    $response = [
        'status' => 'FAIL',
        'message' => $dbConfig['false'],
    ];
}
echo json_encode($response);