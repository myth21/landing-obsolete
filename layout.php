<!DOCTYPE html>
<html lang="<?php echo $page['lang']; ?>">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo $page['title']; ?></title>
    <style><?php echo $style; ?></style>
    <script><?php echo $headJs; ?></script>
</head>
<body>
<div class="wrap">
<?php foreach ($rows as $row) { ?>
    <div id="<?php echo $row['rowId'] ?: ''; ?>" class="row <?php echo $row['rowClass'] ?: ''; ?>" style="<?php echo $row['rowStyle'] ?? ''; ?>">
        <div class="box <?php echo $row['boxClass'] ?: ''; ?>" style="<?php echo $row['boxStyle'] ?: ''; ?>">
            <?php echo $row['content'] ?: ''; ?>
        </div>
    </div>
<?php } ?>
</div>
<script><?php echo $bodyJs; ?></script>
</body>
</html>